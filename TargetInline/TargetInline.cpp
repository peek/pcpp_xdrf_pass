//===- Identify xDRF regions in non-structured parallel applications ------===//
//
//
//===----------------------------------------------------------------------===//
//
//
//===----------------------------------------------------------------------===//

#include <sstream>
#include <iostream>
#include <string>
#include <stack>
#include <set>
#include <queue>
#include <list>
#include <map>
#include <utility>
#include <algorithm>
#include <map>


#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Debug.h"

#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/APInt.h"

#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/NoFolder.h"


#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/CFG.h"
#include "llvm/Analysis/AliasAnalysis.h"

#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"

#include "llvm/Pass.h"
//#include "llvm/Support/InstIterator.h"


//#include "../Utils/SkelUtils/CallingDAE.cpp"
//#include "../Utils/SkelUtils/MetadataInfo.h"

#define LIBRARYNAME "TargetInline"
#define PRINTSTREAM errs() // raw_ostream

#define PRINT_INLINE PRINTSTREAM << "TargetInline: "
#define PRINT_REMOVE PRINTSTREAM << "RemoveInlined: "
#define PRINT_SANITY PRINTSTREAM << "SanityCheck: "

#define DEBUG_INLINE(X) DEBUG_WITH_TYPE("target-inline", PRINT_INLINE << X)
#define DEBUG_REMOVE(X) DEBUG_WITH_TYPE("remove-inlined", PRINT_REMOVE << X)
#define DEBUG_SANITY(X) DEBUG_WITH_TYPE("sanity-check", PRINT_SANITY << X)
using namespace llvm;
using namespace std;

static cl::opt<bool> InlineAll(
                               "inline-all",
                               cl::desc("Marks everything that is not a targeted function for inlining"));

namespace {

    //Verifies that a CallSite is not null-initialized
    bool isNotNull(CallSite call) {
        return call.isCall() || call.isInvoke();
    }

    //Verifies that an instruction can be a callsite
    bool isCallSite(Instruction* inst) {
        return isNotNull(CallSite(inst));
    }

    //Gets the called function of a callsite irregardless of casts
    Function *getStripCall(CallSite *callsite) {
        Value *toReturn = callsite->getCalledValue()->stripPointerCasts();
        return dyn_cast<Function>(toReturn);               
    }

    //Convenience function
    Function *getStripCall(Instruction *potentialCall) {
        if (isCallSite(potentialCall)) {
            CallSite callsite(potentialCall);
            return getStripCall(&callsite);
        }
        return NULL;
    }

    bool isMain(Function &F) {
        return F.getName().str().compare("main") == 0;
    }


    struct SanityCheck : public ModulePass {
        static char ID;
        SanityCheck() : ModulePass(ID) {}

    public:
        int DR_ID;
        virtual void getAnalysisUsage(AnalysisUsage &AU) const{
            //AU.addRequired<DominatorTreeWrapperPass>();
            //AU.addRequired<AliasAnalysis>();
            //AU.addRequired<LoopInfoWrapperPass>();
        }

        virtual bool runOnModule(Module &M) {
            // Find the functions that are spawned by pthread_create
            // These functions can be threads
            SmallPtrSet<Function*,16> thrdFunctions;
            assert(M.getFunction("pthread_create") && "Module does not spawn threads.");
            Function *pthCreate = M.getFunction("pthread_create");
            for (auto pt = pthCreate->users().begin(); pt != pthCreate->users().end(); ++pt) {
                if (isa<CallInst>(*pt)) {
                    CallInst *callsite = dyn_cast<CallInst>(*pt); // pthread_create call
                    Value *funcOp = callsite->getArgOperand(2); // operand for spawned function
                    Value *stripFuncOp = funcOp->stripPointerCasts();
                    assert(Function::classof(stripFuncOp) && "Could not dereive function");
                    Function *spawnFunc = dyn_cast<Function>(stripFuncOp); // spawned function
                    DEBUG_SANITY("Function used by pthread_create found: " << spawnFunc->getName() << "\n");
                    thrdFunctions.insert(spawnFunc);
                }
            }

            // Find pthread functions, functions starting with "pthread"
            SmallPtrSet<Function*,16> pthFunctions;
            for (auto mI = M.begin(); mI != M.end(); ++mI) {
                if ((*mI).getName().startswith("pthread")) {
                    pthFunctions.insert(&(*mI));
                }
            }

            // Find functions using pthreads (i.e. calls any of the pthFunctions)
            SmallPtrSet<Function*,128> pthUsers;
            for (auto pI = pthFunctions.begin(); pI != pthFunctions.end(); ++pI) {
                for (auto uI = (*pI)->users().begin(); uI != (*pI)->users().end(); ++uI) {
                    if (CallInst::classof(*uI)) {
                        CallInst *call = (CallInst*) (*uI);
                        Function *pthUser = call->getParent()->getParent();
                        pthUsers.insert(pthUser);
                    }
                }
            }

            // Find any function that has an instruction for which the
            // function that is called cannot be trivially determined
            for (auto fI = M.begin(); fI != M.end(); ++fI) {
                for (auto iI = inst_begin(*fI); iI != inst_end(*fI); ++iI) {
                    if (isCallSite(&*iI) && !getStripCall(&*iI)) {
                        //function call with undetermined target
                        //assume it uses pthreads
                        pthUsers.insert(iI->getParent()->getParent());
                    }
                }
            }

            bool change = false;
            
            deque<Function*> pthUsersQueue(pthUsers.begin(),pthUsers.end());
            SmallPtrSet<Function*,32> visitedFuns;
            
            while (!pthUsersQueue.empty()) { //Function uses pthread,
                Function *mI = pthUsersQueue.front(); pthUsersQueue.pop_front();
                if (visitedFuns.count(&(*mI)) == 0 &&  // We have not marked it previously
                    !isMain(*mI) &&                     // but is not the main function,
                    thrdFunctions.count(&(*mI)) == 0 && // nor is it spawned by pthread_create,
                    !((*mI).empty())                    // and is not externally defined
                                                        //   (this check should be useless,
                                                        //   since external functions cannot
                                                        //   be deteced as using pthread),
                    ) {                                 // then mark it for inlining.
                    visitedFuns.insert(mI);
                    //Add calls to this function in queue.
                    for (auto it = mI->users().begin(), et = mI->users().begin();
                         it != et; ++it) {
                        if (isa<CallInst>(*it) || isa<InvokeInst>(*it))
                            pthUsersQueue.push_back((dyn_cast<Instruction>(*it))->getParent()->getParent());
                    }
                }
            }
            
            for (auto mI = M.begin(), mE = M.end(); mI != mE; ++mI) {
                if (visitedFuns.count(&*mI) != 0) {
                    if ((*mI).hasFnAttribute(Attribute::NoInline)) {
                        DEBUG_SANITY("Detected function with 'NoInline' attribute: " << (*mI).getName() << "\n");
                    }
                    if (((*mI).hasFnAttribute(Attribute::AlwaysInline))) {
                        DEBUG_SANITY("Detected non-inlined function with 'AlwaysInline' attribute: " << (*mI).getName() << "\n");
                    }
                }
            }
            return change;
        }
    };


    struct TargetInline : public ModulePass {
        static char ID;
        TargetInline() : ModulePass(ID) {}

    public:
        int DR_ID;
        virtual void getAnalysisUsage(AnalysisUsage &AU) const{
            //AU.addRequired<DominatorTreeWrapperPass>();
            //AU.addRequired<AliasAnalysis>();
            //AU.addRequired<LoopInfoWrapperPass>();
        }

        virtual bool runOnModule(Module &M) {
            // Find the functions that are spawned by pthread_create
            // These functions can be threads
            SmallPtrSet<Function*,16> thrdFunctions;
            assert(M.getFunction("pthread_create") && "Module does not spawn threads.");
            Function *pthCreate = M.getFunction("pthread_create");
            for (auto pt = pthCreate->users().begin(); pt != pthCreate->users().end(); ++pt) {
                if (isa<CallInst>(*pt)) {
                    CallInst *callsite = dyn_cast<CallInst>(*pt); // pthread_create call
                    Value *funcOp = callsite->getArgOperand(2); // operand for spawned function
                    Value *stripFuncOp = funcOp->stripPointerCasts();
                    assert(Function::classof(stripFuncOp) && "Could not dereive function");
                    Function *spawnFunc = dyn_cast<Function>(stripFuncOp); // spawned function
                    DEBUG_INLINE("Function used by pthread_create found: " << spawnFunc->getName() << "\n");
                    thrdFunctions.insert(spawnFunc);
                }
            }

            // Find pthread functions, functions starting with "pthread"
            SmallPtrSet<Function*,16> pthFunctions;
            for (auto mI = M.begin(); mI != M.end(); ++mI) {
                if ((*mI).getName().startswith("pthread")) {
                    pthFunctions.insert(&(*mI));
                }
            }

            // Find functions using pthreads (i.e. calls any of the pthFunctions)
            SmallPtrSet<Function*,128> pthUsers;
            for (auto pI = pthFunctions.begin(); pI != pthFunctions.end(); ++pI) {
                for (auto uI = (*pI)->users().begin(); uI != (*pI)->users().end(); ++uI) {
                    if (CallInst::classof(*uI)) {
                        CallInst *call = (CallInst*) (*uI);
                        Function *pthUser = call->getParent()->getParent();
                        pthUsers.insert(pthUser);
                    }
                }
            }

            bool change = false;
            
            deque<Function*> pthUsersQueue(pthUsers.begin(),pthUsers.end());
            SmallPtrSet<Function*,32> visitedFuns;
            
            while (!pthUsersQueue.empty()) { //Function uses pthread,
                Function *mI = pthUsersQueue.front(); pthUsersQueue.pop_front();
                errs() << "Visited " << mI->getName() << "\n"; 
                if (visitedFuns.count(&(*mI)) == 0 &&  // We have not marked it previously
                    !isMain(*mI) &&                     // but is not the main function,
                    thrdFunctions.count(&(*mI)) == 0 && // nor is it spawned by pthread_create,
                    !((*mI).empty())                    // and is not externally defined
                                                        //   (this check should be useless,
                                                        //   since external functions cannot
                                                        //   be deteced as using pthread),
                    ) {                                 // then mark it for inlining.
                    visitedFuns.insert(mI);
                    //Add calls to this function in queue.
                    for (auto it = mI->users().begin(), et = mI->users().end();
                         it != et; ++it) {
                        if (isa<CallInst>(*it) || isa<InvokeInst>(*it))
                            pthUsersQueue.push_back((dyn_cast<Instruction>(*it))->getParent()->getParent());
                    }
                }
            }
            
            for (auto mI = M.begin(), mE = M.end(); mI != mE; ++mI) {
                if (visitedFuns.count(&*mI) != 0 || InlineAll) {
                    if ((*mI).hasFnAttribute(Attribute::NoInline)) {
                        (*mI).removeFnAttr(Attribute::NoInline);
                        DEBUG_INLINE("In function " << (*mI).getName() << ": - NoInline\n");
                        change = true;
                    } else {
                        DEBUG_INLINE("In function " << (*mI).getName() << ": | NoInline\n");
                    }
                    if (!((*mI).hasFnAttribute(Attribute::AlwaysInline))) {
                        (*mI).addFnAttr(Attribute::AlwaysInline);
                        DEBUG_INLINE("In function " << (*mI).getName() << ": + AlwaysInline\n");
                        change = true;
                    } else {
                        DEBUG_INLINE("In function " << (*mI).getName() << ": | AlwaysInline\n");
                    }
                }
                else {
                    DEBUG_INLINE("Skipped function: " << (*mI).getName() << "\n");
                }
            }
            return change;
        }
    };



    struct RemoveInlined : public ModulePass {
        static char ID;
        RemoveInlined() : ModulePass(ID) {}

    public:
        int DR_ID;
        virtual void getAnalysisUsage(AnalysisUsage &AU) const{
            //AU.addRequired<DominatorTreeWrapperPass>();
            //AU.addRequired<AliasAnalysis>();
            //AU.addRequired<LoopInfoWrapperPass>();
        }

        virtual bool runOnModule(Module &M) {
            bool change = false;
            Function *remove = NULL;
            do {
                remove = NULL;
                for (auto mI = M.begin(); mI != M.end() && remove == NULL; ++mI) {
                    if (!isMain(*mI) && !((*mI).hasNUsesOrMore(1))) {
                        remove = &(*mI);
                    }
                }
                if (remove != NULL) {
                    DEBUG_REMOVE("Removing function: " << remove->getName() << "\n");
                    remove->eraseFromParent();
                    change = true;
                }
            } while (remove != NULL);

            // Check if all always inline functions were removed
            for (auto mI = M.begin(); mI != M.end(); ++mI) {
                if ((*mI).hasFnAttribute(Attribute::AlwaysInline)) {
                    DEBUG_REMOVE("Could not remove always-inline function: " << (*mI).getName() << "\n");
                    DEBUG_REMOVE("  Uses:\n");
                    for (auto uI = (*mI).users().begin(); uI != (*mI).users().end(); ++uI) {
                        //DEBUG_REMOVE("    " << **uI << "\n");
                    }
                }
            }
            return change;
        }

    };

}


char TargetInline::ID = 0;
static RegisterPass<TargetInline> X("target-inline",
                                    "Mark targets as always inline",
                                    false,
                                    false);

char RemoveInlined::ID = 0;
static RegisterPass<RemoveInlined> Y("remove-inlined",
                                     "Removes unused functions in the module",
                                     false,
                                     false);

char SanityCheck::ID = 0;
static RegisterPass<SanityCheck> Z("sanity-check",
                                   "Verifies that target- and remove-inlined did their jobs correctly",
                                   false,
                                   false);


/* Local Variables: */
/* mode: c++ */
/* indent-tabs-mode: nil */
/* c-basic-offset: 4 */
/* End: */
