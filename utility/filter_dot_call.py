import sys

f = open(sys.argv[1], 'r')
content = f.read()
f.close()
lines = content.split('\n')

for l in lines:
    if '\\l' in l:
        segs = l.split('\\l')
        pr = segs[:1]
        i = 1
        while i < len(segs):
            if segs[i][0:2] == '  ' or segs[i][0:3] == '...':
                # code
                if 'call' in segs[i]:
                    pr.append(segs[i]) 
                    while segs[i+1][0:3] == '...':
                        # continuation
                        i += 1
                        pr.append(segs[i])
            else:
                # other
                pr.append(segs[i])
            i += 1
        print '\\l'.join(pr)
    else:
        print l
