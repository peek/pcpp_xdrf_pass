#!/bin/bash

xDRFpassSo=$XDRF_BUILD/xDRFpass/libxDRFpass.so

if [ ! -e $xDRFpassSo ] ; then
    echo "Could not find xDRFpass pass, make sure you have setup the env and compiled the passes"
    exit 1
fi

if [[ $# < 1 ]] ; then
    echo "Requires atleast one argument, the input .ll file"
    exit 1
fi

targetFile=$1

if [[ $# > 1 ]] ; then
    outputFile=$2
else
    outputFile=$targetFile.ll
fi

opt -S -load $xDRFpassSo -split-calls $targetFile -o $outputFile
