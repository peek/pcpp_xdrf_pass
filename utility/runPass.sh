#!/bin/bash

TargetInlineSo=$XDRF_BUILD/TargetInline/libTargetInline.so
xDRFpassSo=$XDRF_BUILD/xDRFpass/libxDRFpass.so

if [ ! -e $TargetInlineSo ] ; then
    echo "Could not find TargetInline pass, make sure you have setup the env and compiled the passes"
    exit 1
fi

if [ ! -e $xDRFpassSo ] ; then
    echo "Could not find xDRFpass pass, make sure you have setup the env and compiled the passes"
    exit 1
fi

if [[ $# < 1 ]] ; then
    echo "Requires atleast one argument, the input .ll file"
    exit 1
fi

targetFile=$1

if [[ $# > 1 ]] ; then
    outputFile=$2
else
    outputFile=$targetFile.ll
fi

opt -S -load $TargetInlineSo -load $xDRFpassSo \
    -debug-only=target-inline -debug-only=remove-inlined \
       -target-inline -always-inline -remove-inlined -O3 -split-calls\
       -basicaa -globalsmodref-aa -scalar-evolution -scev-aa -tbaa -cfl-aa -xDRF -noalias-dgep -constbase-dgep\
    $targetFile -o $outputFile
    
