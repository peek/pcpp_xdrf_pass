#!/bin/bash

# This script will perform step 1 from the install instructions.
# For best effect, enter an empty directory where you want LLVM
# to live, i.e. <llvm-prefix>.

# The script will download necessary source code with wget,
# unpack it using tar, configure the build, and optionally perform
# the build and install. If you wish to run the build faster
# by passing the -j flag (see install_instructions.txt), please
# choose to perform the build and install manually using:
#   cd <llvm-prefix>/llvm-3.7.0-build
#   cmake --build . [ -- [extra options]]
#   cmake --build . --target install

# Run this script by:
#   cd <llvm-prefix>
#   bash <path to this script>/get-llvm.sh


# Remember <llvm-prefix>
LLVM_PREFIX="$(pwd)"

# Download source code
wget http://llvm.org/releases/3.7.0/llvm-3.7.0.src.tar.xz
wget http://llvm.org/releases/3.7.0/cfe-3.7.0.src.tar.xz
wget http://llvm.org/releases/3.7.0/compiler-rt-3.7.0.src.tar.xz

# Unpack and move code
tar xvf llvm-3.7.0.src.tar.xz
tar xvf cfe-3.7.0.src.tar.xz
tar xvf compiler-rt-3.7.0.src.tar.xz
mv llvm-3.7.0.src "$LLVM_PREFIX/llvm-3.7.0-src"
mv cfe-3.7.0.src "$LLVM_PREFIX/llvm-3.7.0-src/tools/clang"
mv compiler-rt-3.7.0.src "$LLVM_PREFIX/llvm-3.7.0-src/projects/compiler-rt"

# Make build directory and configure
mkdir "$LLVM_PREFIX/llvm-3.7.0-build"
cd "$LLVM_PREFIX/llvm-3.7.0-build"
cmake -DCMAKE_INSTALL_PREFIX="$LLVM_PREFIX/llvm-3.7.0-install" \
      -DLLVM_ENABLE_ASSERTIONS=ON \
      -DCMAKE_BUILD_TYPE=Release \
      -DLLVM_ENABLE_RTTI=ON \
      "$LLVM_PREFIX/llvm-3.7.0-src"

# Ask if the code should be built
echo "Code is now ready to be built. This may take a while."
read -p "Do you want to build it now? [y/N] " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # Build and install
    cmake --build .
    cmake --build . --target install
fi
