#!/usr/bin/python

import re
import sys

lines = sys.stdin.readlines()

symbols = [
    ('ier',     " Incorrectly enclave nDRFs"),
    ('iner',    " Incorrectly non-enclave nDRFs"),
    ('inber',   " Incorrectly non-barrier enclave nDRFs"),
    ('inebr',   " Incorrectly non-enclave barrier nDRFs"),
    ('menrms',  " Marked enclave nDRFs with no RMS call"),
    ('mnenrms', " Marked non-enclave nDRFs with no RMS call"),
    ('mbnrms',  " Marked barrier nDRFs with no RMS call"),
    ('xrmsnm',  " XDRF RMS locks with no marking"),
    ('nxrmsnm', " nXDRF RMS locks with no marking"),
    ('brmsnm',  " RMS barriers with no marking"),
    ('ssnm',    " Semaphore signals with no marking"),
    ('swnm',    " Semaphore waits with no marking"),
    ('mbnbnerms', " Marked barrier nDRFs with non-barrier non-enclave RMS call"),
    ('mnenbbrms', " Marked non-enclave nDRFs with barrier RMS call"),
    ('cer',     " Correctly marked enclave NDRFs"),
    ('cner',    " Correctly marked non-enclave NDRFs"),
    ('cbr',     " correctly marked barrier NDRFs"),
    ('nr',      " Num of marked nDRF regions"),
    ('ner',     " Num of enclave nDRF regions"),
    ('nbr',     " Num of found barriers"),
    ]

stat = {}

def getNumber(line):
    return int(line.split()[-1])

def extractStat(line):
    for k,s in symbols:
        if s in line:
            stat[k] = getNumber(line)

for line in lines:
    res = re.search("([A-Z]+)\.xDRF\.ll",line)
    if res:
        print res.group(1).lower() , ",",
        break

for line in lines:
    extractStat(line)

print stat['nr'] , ",",
#Enclave stats
print stat['cer'] , ",",
print stat['ier'] , ",",
print stat['inber'] , ",",
print stat['menrms'] , ",",
#Non-enclave stats
print stat['cner'] , ",",
print stat['iner'] , ",",
print stat['mnenbbrms'] , ",",
print stat['mnenrms'] , ",",
#Barrier stats
print stat['cbr'] , ",",
print stat['inebr'], ",",
print stat['mbnbnerms'] , ",",
print stat['mbnrms'] , ",",
#RMS unaligned stats
print stat['xrmsnm'] + stat['nxrmsnm'] + stat['brmsnm'] + stat['ssnm'] + stat['swnm'], ",",
print stat['xrmsnm'] , ",",
print stat['nxrmsnm'] , ",",
print stat['brmsnm'] , ",",
print stat['ssnm'] , "," ,
print stat['swnm'] , "," ,
