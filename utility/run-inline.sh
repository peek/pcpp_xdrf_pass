#!/bin/bash

TargetInlineSo=$XDRF_BUILD/TargetInline/libTargetInline.so

if [ ! -e $TargetInlineSo ] ; then
    echo "Could not find TargetInline pass, make sure you have setup the env and compiled the passes"
    exit 1
fi

if [[ $# < 1 ]] ; then
    echo "Requires atleast one argument, the input .ll file"
    exit 1
fi

targetFile=$1

if [[ $# > 1 ]] ; then
    outputFile=$2
    shift
else
    outputFile=$targetFile.ll
fi
shift

opt -O3 $targetFile | \
opt -S -load $TargetInlineSo \
    -debug-only=target-inline -debug-only=remove-inlined -debug-only=sanity-check \
    -target-inline -always-inline -O3 -remove-inlined -sanity-check $@\
    -o $outputFile
