//===- Identify xDRF regions in non-structured parallel applications ------===//
//
//
//===----------------------------------------------------------------------===//
//
//
//===----------------------------------------------------------------------===//

#include <sstream>
#include <iostream>
#include <string>
#include <stack>
#include <set>
#include <queue>
#include <list>
#include <map>
#include <utility>
#include <algorithm>
#include <map>

#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"

#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/APInt.h"

#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/NoFolder.h"

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/CFG.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/MemoryLocation.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"

#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"

#include "llvm/Pass.h"
//#include "llvm/Support/InstIterator.h"


//#include "../Utils/SkelUtils/CallingDAE.cpp"
//#include "../Utils/SkelUtils/MetadataInfo.h"

//#define DEBUG_PRINT
#ifdef DEBUG_PRINT
#define DEBUG_PRINT_SOFT
#endif

#define DEBUG_PRINT_SOFT

#define LIBRARYNAME "xDRF"
#define PRINTSTREAM errs() // raw_ostream

#define PT_LOCK_POST ".lock"
#define PT_UNLOCK_POST ".unlock"




using namespace llvm;
using namespace std;

static cl::opt<bool> SplitLibrary(
                                  "split-lib",
                                  cl::desc("Makes library calls into nDRF regions"));

static cl::opt<bool> AssumeDGEPNoAlias(
                                       "noalias-dgep",
                                       cl::desc("Assumes that dynamic GEP instructions that do not have a constant base in ScalarEvolution never alias"));

static cl::opt<bool> AssumeDGEPAliasConstBase(
                                              "constbase-dgep",
                                              cl::desc("Assumes that dynamic GEP instructions can be resolved into a constant offset if all their indices have a constant base in ScalarEvolution"));

static cl::opt<bool> SignalWaitSane(
                                    "signalwait-sane",
                                    cl::desc("Assumes locks containing signal/wait do not internally synchronize program flow"));

static cl::opt<bool> FullyCheck(
                                "fully-check",
                                cl::desc("Does not terminate conflict checking upon finding a conflict."));

static cl::opt<bool> OptimisticLooseNDRF(
                                         "optimistic-loose",
                                         cl::desc("Does not assume nDRF regions with unknown function calls to be non-enclave"));

#define NUM_VERSIONS 4
static cl::opt<bool> MultiVersioning(
                                     "multi-version",
                                     cl::desc("Generates multiple versions of xDRF delineation at the same time"));
int curr_version;


namespace {
    //If the pass defaults to splitting around library calls, you can specify
    //functions here that will NOT be split on.
    set<StringRef> whiteList = {
        // "printf", //Maybe?
        // "fprintf", //Maybe?
        // "puts", //Maybe?
        // "putchar", //Maybe?
        // "time", //SHOULD be read-only
        // //"exit" //Maybe?,
        // "getopt", //Maybe? 
        // "strtol",
        // "sin",
        // "cos"
    };

    //If the pass defaults to not splitting around library calls, you
    //can specify functions here that WILL be split on
    set<StringRef> blackList = {
        "_foreach_patch",
        "foreach_leaf_edge",
        "g_start"
    };

    //Verifies that a CallSite is not null-initialized
    bool isNotNull(CallSite call) {
        return call.isCall() || call.isInvoke();
    }

    //Verifies that an instruction can be a callsite
    bool isCallSite(Instruction* inst) {
        return isNotNull(CallSite(inst));
    }

    //Gets the called function of a callsite irregardless of casts
    Function *getStripCall(CallSite *callsite) {
        Value *toReturn = callsite->getCalledValue()->stripPointerCasts();
        return dyn_cast<Function>(toReturn);               
    }

    //Convenience function
    Function *getStripCall(Instruction *potentialCall) {
        if (isCallSite(potentialCall)) {
            CallSite callsite(potentialCall);
            return getStripCall(&callsite);
        }
        return NULL;
    }

    void findTargetFunctions(Module &M, SmallPtrSet<Function*,16> &targetFunctions) {
        assert(M.getFunction("pthread_create") && "Module does not spawn threads.");
        Function *pthCreate = M.getFunction("pthread_create");
        for (auto pt = pthCreate->users().begin(); pt != pthCreate->users().end(); ++pt) {  
            if (auto inst = dyn_cast<Instruction>(*pt)) {
                //if (isa<CallInst>(*pt)) {
                if (isCallSite(inst)) {
                    CallInst *callsite = dyn_cast<CallInst>(*pt);
                    Value *funcOp = callsite->getArgOperand(2);
                    //Try to resolve the value into a proper function
                    while (!isa<Function>(funcOp)) {
                        if (ConstantExpr::classof(funcOp)) {
                            ConstantExpr *constOp = dyn_cast<ConstantExpr>(funcOp);
                            if (constOp->isCast()) {
                                funcOp = constOp->getAsInstruction();
                                CastInst *castOp = dyn_cast<CastInst>(funcOp);
                                Value* tempOp = funcOp;
                                funcOp = castOp->getOperand(0);
                                delete(tempOp);
                            }
                        } else if (CastInst::classof(funcOp)) {
                            CastInst *castOp = dyn_cast<CastInst>(funcOp);
                            funcOp = castOp->getOperand(0);
                        }
                        else { //Unable To Resolve
                            errs() << "Failed to resolve third argument of pthread_create: "
                                   << *funcOp << "\n  which has type: " << typeid(*funcOp).name() << "\n";
                            break;
                        }
                    }
                    if (isa<Function>(funcOp)) {
                        Function *spawnFunc = dyn_cast<Function>(funcOp);
#ifdef DEBUG_PRINT
                        errs() << "Targeting function used by pthread_create: " << spawnFunc->getName() << "\n";
#endif
                        targetFunctions.insert(spawnFunc);
                    }
                }
            }
        }
        assert(M.getFunction("main"));
        targetFunctions.insert(M.getFunction("main"));
        assert(targetFunctions.count(M.getFunction("main")) && "Main is not in target???");
    }

    
    map<Function*,pair<SmallPtrSet<Instruction*,256>,
                       SmallPtrSet<Function*,16> > > getExecutableInstsDynamic;
    
    pair<SmallPtrSet<Instruction*,256>,
         SmallPtrSet<Function*,16> > getExecutableInsts(Function* fun) {
        assert(fun && "Tried to get executable instructions of a null ptr");
        if (getExecutableInstsDynamic.count(fun) == 0) {
            SmallPtrSet<Instruction*,256> newInstSet;
            SmallPtrSet<Function*,16> newFunSet;
            for (inst_iterator it = inst_begin(fun),
                     et = inst_end(fun);
                 it != et; ++it) {
                newInstSet.insert(&(*it));
                //if (isa<CallInst>(*it)) {
                if (isCallSite(&*it)) {
                    //CallInst *callInst = &(cast<CallInst>(*it));
                    CallSite call = CallSite(&*it);
                    Function *calledFun = getStripCall(&call);
                        //callInst->getCalledFunction();
                    
                    if (calledFun != fun) {
                        if (calledFun && !calledFun->empty())
                            newFunSet.insert(calledFun);
                    }
                }
            }
            getExecutableInstsDynamic[fun] =
                make_pair(newInstSet,newFunSet);
        }
        return getExecutableInstsDynamic[fun];
    }
    
    struct SplitPThread : public ModulePass {
        static char ID;
        SplitPThread() : ModulePass(ID) {}
    public:
        virtual void getAnalysisUsage(AnalysisUsage &AU) const{
            AU.addRequired<LoopInfoWrapperPass>();
        }
        
        SmallPtrSet<Function*,128> pthUsers;

        virtual bool runOnModule(Module &M) {

            // Find pthread functions, functions starting with "pthread"
            SmallPtrSet<Function*,16> pthFunctions;
            for (auto mI = M.begin(); mI != M.end(); ++mI) {
                if ((*mI).getName().startswith("pthread")) {
                    pthFunctions.insert(&(*mI));
                }
            }

            // Find functions using pthreads (i.e. calls any of the pthFunctions)
            for (auto pI = pthFunctions.begin(); pI != pthFunctions.end(); ++pI) {
                for (auto uI = (*pI)->users().begin(); uI != (*pI)->users().end(); ++uI) {
                    if (CallInst::classof(*uI)) {
                        CallInst *call = (CallInst*) (*uI);
                        Function *pthUser = call->getParent()->getParent();
                        pthUsers.insert(pthUser);
                    }
                }
            }
            
            deque<Function*> pthUserQueue(pthUsers.begin(),pthUsers.end());
            while (!pthUserQueue.empty()) {
                Function *currfun = pthUserQueue.front(); pthUserQueue.pop_front();
                for (auto uI = currfun->users().begin();
                     uI != currfun->users().end(); ++uI) {
                    if (CallInst::classof(*uI)) {
                        CallInst *call = (CallInst*) (*uI);
                        Function *pthUser = call->getParent()->getParent();
                        if (pthUsers.count(pthUser)==0) {
                            pthUsers.insert(pthUser);
                            pthUserQueue.push_back(pthUser);
                        }
                    }
                }
            }

            errs() << "Pth users:\n";
            for (Function* fun : pthUsers) {
                errs() << fun->getName() << "\n";
                
            }

            //Find the functions that are spawned on by pthread_create
            findTargetFunctions(M,targetFunctions);
            bool change = false;
            for (auto fI = targetFunctions.begin(), fE = targetFunctions.end();  
                 fI != fE; ++fI) {
                if ((*fI)->empty()) continue;
                change = splitBlocksAroundPthreadsCalls(*fI) || change;
            }
            errs() << "Split around " << splitcount << " pthread calls\n";
            return change;
        }
    private:
        SmallPtrSet<Function*,16> targetFunctions;

        int splitcount = 0;
        
        bool splitBlocksAroundPthreadsCalls(Function *F) {
            errs() << "Splitting calls in function: " << F->getName() << "\n";
            Function::iterator bi = F->begin();
            bool changed = false;
            bool iter_changed = false;
            while (bi != F->end()){
                BasicBlock::iterator ii = bi->begin();
                while ( ii != bi->end()){
                    iter_changed = false;
                    if (isCallSite(&(*ii))) {
                        bool splitCall = false;
                        //If we can find the function, handle it
                        if (Function *fun = getStripCall(&(*ii))) {
                            // split before pthread_mutex_lock
                            if (fun->getName().str().compare("pthread_mutex_lock") == 0){
                                LoopInfo *LI = &getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
                                BasicBlock* newBB = splitBefore(&(*ii), LI, PT_LOCK_POST);
                                ii = newBB->begin();
                                bi = newBB;
                                changed = true;
                                iter_changed = true;
                                // bypass the lock call
                                ii++;
                            }
                            // split after pthread_mutex_unlock
                            else if (fun->getName().str().compare("pthread_mutex_unlock") == 0){
                                ii++;
                                LoopInfo *LI = &getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
                                BasicBlock* newBB = splitBefore(&(*ii), LI, PT_UNLOCK_POST);
                                ii = newBB->begin();
                                bi = newBB;
                                changed = true;
                                iter_changed = true;
                            }
                            //Here we split out "unary" nDRF regions, e.g. regions that begin and end around
                            //one instruction. All pthread calls that are not init calls are
                            //split out. All remaining non-library calls are split out
                            //Library calls are split out depending on mode
                            else {
                            
                                if ((fun->getName().startswith("pthread_") &&
                                     !(fun->getName().endswith("_init"))))
                                    splitCall = true;
                                if (fun->getName().equals("pthread_self"))
                                    splitCall = false;
                                if ((fun->getName().startswith("sem_") &&
                                     !(fun->getName().endswith("_init"))))
                                    splitCall = true;
                                if (! (fun->empty()) ) {
                                    //errs() << "Split around " << fun->getName() << "?\n";
                                    splitCall = pthUsers.count(fun) != 0;
                                    //errs() << splitCall << "\n";
                                }
                                //Only split on the call if it top-level uses a pthread call
                                if (SplitLibrary && whiteList.count(fun->getName()) == 0)
                                    splitCall = true;
                                if (!SplitLibrary && blackList.count(fun->getName()) != 0)
                                    splitCall = true;
                                //Don't split on calls which are the entry points for functions
                                if (targetFunctions.count(fun) != 0)
                                    splitCall = false;
                            }
                            //If we cannot determine what was called, we must split on the call
                        } else {
                            splitCall=true;
                        }
                        if (splitCall) {
                            ++splitcount;
                            LoopInfo *LI = &getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
                            BasicBlock* newBB = splitBefore(&(*ii), LI, PT_LOCK_POST);
                            // #ifdef DEBUG_PRINT_SOFT
                            //                                 errs() << "Split around library call: " << *ii << "\n";
                            // #endif
                            ii = newBB->begin();
                            bi = newBB;
                            ii++;
                            newBB = splitBefore(&(*ii), LI, PT_UNLOCK_POST);
                            ii = newBB->begin();
                            bi = newBB;
                            changed = true;
                            iter_changed = true;
                        }
                    }
                    if (!iter_changed) ii++;
                }
                bi++;
            }
            /*if (changed)
              F->setName(F->getName()+xDRFstr);*/
            return changed;
        }
        
        BasicBlock* splitBefore(Instruction *SplitBefore, LoopInfo *LI, string suf) {
            BasicBlock *SplitMe = SplitBefore->getParent();
            BasicBlock *NewBB = SplitMe->splitBasicBlock(
                                                         BasicBlock::iterator(SplitBefore));
            NewBB->setName(SplitMe->getName() + suf);
            Loop* L = LI->getLoopFor(SplitMe);
            if (L) L->addBasicBlockToLoop(NewBB, *LI);
            return NewBB;
        }   
    };

    struct xDRF : public ModulePass {
        static char ID;
        xDRF() : ModulePass(ID) {}

    public:
        int DR_ID;
        virtual void getAnalysisUsage(AnalysisUsage &AU) const{
            //AU.addRequired<DominatorTreeWrapperPass>();
            AU.addRequired<AliasAnalysis>();
            AU.addRequired<ScalarEvolution>();
            AU.addRequired<TargetLibraryInfoWrapperPass>();
            AU.addRequired<LoopInfoWrapperPass>();
        }

        //Assume input module is inlined as far as possible
        //

        SmallPtrSet<Function*,16> targetFunctions;

        virtual bool runOnModule(Module &M) {
            wM = &M;
            //Find the functions that are spawned on by pthread_create

            findTargetFunctions(M,targetFunctions);

            errs() << "Targeting:\n";
            for (Function* fun : targetFunctions)
                errs() << fun->getName() << "\n";

            Function *nDRFBeginMark, *nDRFEndMark,
                *BarrierEndMark,
                *xDRFBeginMark, *xDRFEndMark;

            setupDummyCalls(nDRFBeginMark, nDRFEndMark,
                            BarrierEndMark,
                            xDRFBeginMark, xDRFEndMark, M);

            //Proceed with pass (change = true since we added a function)
            AA = &getAnalysis<AliasAnalysis>();
            TLI = &getAnalysis<TargetLibraryInfoWrapperPass>();

            DR_ID = 0;
            bool change = true;

            SmallPtrSet<SyncVar*,16> syncvars;
            SmallPtrSet<NDRFRegion*,128> allRegions;
            for (auto fI = targetFunctions.begin(), fE = targetFunctions.end();  
                 fI != fE; ++fI) {
                //errs() << "F";
                //errs().flush();
                Function *func_orig = *fI;
                if (func_orig->empty()) continue;
                if (func_orig == nDRFBeginMark || func_orig == nDRFEndMark ||
                    func_orig == xDRFBeginMark || func_orig == xDRFEndMark)
                    continue;
                SmallPtrSet<NDRFRegion*,32> regions;
                delimitRegions(*fI,regions,syncvars);
                insertDummySyncVars(regions,syncvars);
                checkForBarriers(regions);
                findPaths(regions);
                for (int i = 0; i < NUM_VERSIONS; ++i)
                    createIndexedCall(xDRFBeginMark,&(func_orig->front().front()),i);
                //Find all exit points and end the xDRF region there
                for (auto bit = func_orig->begin(),
                         bet = func_orig->end(); bit != bet; ++bit) {
                    if (ReturnInst::classof((*bit).getTerminator()))
                        for (int i = 0; i < NUM_VERSIONS; ++i)
                            createIndexedCall(xDRFEndMark,(*bit).getTerminator(),i);
                }
                allRegions.insert(regions.begin(),regions.end());
            }
            errs() << "\n";
            for (curr_version = 0;
                 curr_version < NUM_VERSIONS; curr_version++) {
                if (MultiVersioning) {
                    switch (curr_version) {
                        //conservative
                    case 0:
                        AssumeDGEPNoAlias.setValue(false);
                        AssumeDGEPAliasConstBase.setValue(false);
                        OptimisticLooseNDRF.setValue(false);
                        break;
                        //optimistic-pa
                    case 1:
                        AssumeDGEPNoAlias.setValue(true);
                        AssumeDGEPAliasConstBase.setValue(true);
                        OptimisticLooseNDRF.setValue(false);
                        break;
                        //optimistic-if
                    case 2:
                        AssumeDGEPNoAlias.setValue(false);
                        AssumeDGEPAliasConstBase.setValue(false);
                        OptimisticLooseNDRF.setValue(true);
                        break;
                        //optimistic-paif
                    case 3:
                        AssumeDGEPNoAlias.setValue(true);
                        AssumeDGEPAliasConstBase.setValue(true);
                        OptimisticLooseNDRF.setValue(true);
                        break;
                    }
                }

                errs() << "Running an analysis with:\n";
                errs() << "Optimistic PA: " << (AssumeDGEPNoAlias ? "true" : "false") << "\n";
                errs() << "Optimistic IF: " << (OptimisticLooseNDRF ? "true" : "false") << "\n";
            
                extendSynchronizations(syncvars);

                if (MultiVersioning) {
                    for (NDRFRegion * reg : allRegions) {
                        errs() << "Region " << reg->rID << " was " <<
                            (reg->isEnclave() ? "enclave" : "not enclave")
                               << " in version " << curr_version << "\n";
                        reg->enclave_version[curr_version]=reg->isEnclave();
                    }
                    for (SyncVar * sync : syncvars) {
                        sync->enclave=false;
                    }
                    visitedAliasValues.clear();
                    pointerAliasDynamic.clear();
                    visitedBottomLevelValues.clear();
                } else {
                    for (NDRFRegion * reg : allRegions) {
                        errs() << "Region " << reg->rID << " was " <<
                            (reg->isEnclave() ? "enclave" : "not enclave")
                               << "\n";
                    }
                    break;
                }
            }
            //contractSyncVarEnclave(allRegions);
            markRegions(nDRFBeginMark, nDRFEndMark,
                        BarrierEndMark,
                        xDRFBeginMark, xDRFEndMark, allRegions);
            //printRegionInfo(allRegions);
            printSyncVarInfo(syncvars);

            for (NDRFRegion * reg : allRegions)
                delete(reg);
            for (SyncVar * sync : syncvars)
                delete(sync);
            
            //errs() << "Created " << allRegions.size() << " nDRF regions\n";

            errs() << "Dynamic index failures: " << numDynamicFailures << "\n";
            errs() << "Argument/Callsite failures: " << numArgumentCallsiteFailures << "\n";
            
            return change;
        }

    private:

        Module* wM;

        AliasAnalysis *AA;
        map<Function*,ScalarEvolution*> SEmap;
        TargetLibraryInfoWrapperPass *TLI;
        
        ScalarEvolution *getSE(Function* f) {
            if (SEmap.count(f) == 0)
                SEmap[f] = &getAnalysis<ScalarEvolution>(*f);
            return SEmap[f];
        }


        class SyncVar;

        class NDRFRegion {
        public:
            NDRFRegion() {
                static int regionsIDs=0;
                rID = regionsIDs++;
                for (int i = 0; i < NUM_VERSIONS; ++i)
                    enclave_version[i] = false;
            }
            int rID;

            SmallPtrSet<BasicBlock*,8> entryBlocks; //In region
            SmallPtrSet<BasicBlock*,8> exitBlocks; //In region
            SmallPtrSet<BasicBlock*,64> inBlocks; //In region (duh)
            SmallPtrSet<SyncVar*,4> syncVars;
            SmallPtrSet<Instruction*,128> precedingInsts;
            SmallPtrSet<Instruction*,128> followingInsts;
            SmallPtrSet<NDRFRegion*,8> precedingNDRFRegions;
            SmallPtrSet<NDRFRegion*,8> followingNDRFRegions;

            bool isBarrier = false;
            bool hasWait = false;
            bool hasSignal = false;
            bool isSemaphore = false;
            bool forcedNXDRF = false;
            bool speccforcedNXDRF = false;

            bool shouldAnalyze() {
                if (isBarrier)
                    return false;
                if (forcedNXDRF)
                    return false;
                if (speccforcedNXDRF && !OptimisticLooseNDRF)
                    return false;
                return true;
            }
            
            bool isEnclaveVer(int i) {
                return enclave_version[i];
            }

            bool enclave_version[NUM_VERSIONS];
            
            bool isEnclave() {
                if (isBarrier)
                    return false;
                if (forcedNXDRF)
                    return false;
                if (speccforcedNXDRF && !OptimisticLooseNDRF)
                    return false;
                if (isSemaphore)
                    return false;
                if (hasWait)
                    return false;
                if (hasSignal)
                    return false;
                //if (hasWait && SignalWaitSane)
                //    return true;
                for (auto it = syncVars.begin(), eit = syncVars.end();
                     it != eit; ++it) {
                    if (!((*it)->enclave))
                        return false;
                }
                return true;
            }

            //Note, does not merge preceding or following basicblocks or regions
            void merge(NDRFRegion *with) {
#ifdef DEBUG_PRINT
                errs() << "Merging region " << rID << "with region " << with->rID << "\n";
#endif
                entryBlocks.insert(with->entryBlocks.begin(),with->entryBlocks.end());
                exitBlocks.insert(with->exitBlocks.begin(),with->exitBlocks.end());
                inBlocks.insert(with->inBlocks.begin(),with->inBlocks.end());
                for (SyncVar* syncvar: syncVars) {
                    syncvar->syncRegions.erase(with);
                    syncvar->syncRegions.insert(this);
                }
                syncVars.insert(with->syncVars.begin(),with->syncVars.end());
            }
        };

        class SyncVar {
        public:
            SyncVar () {
                static int syncvarIDs=0;
                sID = syncvarIDs++;
            }

            bool enclave = false;

            bool isBarrier = false;
            SmallPtrSet<Value*,64> rawSyncs;
            SmallPtrSet<NDRFRegion*,16> syncRegions;

            int sID;
            
            void merge(SyncVar* mergeWith) {
                rawSyncs.insert(mergeWith->rawSyncs.begin(),
                                mergeWith->rawSyncs.end());
                for (NDRFRegion* reg : mergeWith->syncRegions) {
                    syncRegions.insert(reg);
                    reg->syncVars.erase(mergeWith);
                    reg->syncVars.insert(this);
                }
            }
            
            //Checks wether a value aliases with any value in this
            //syncvariable, adds it if it does
            bool checkAlias(Value* toBeChecked, xDRF *pass) {
                for (auto it = rawSyncs.begin(), eit = rawSyncs.end();
                     it != eit; ++it) {
                    if (pass->mutexAlias_(toBeChecked,(*it)))
                        return true;
                }
                return false;
            }
        };

        SmallPtrSet<Value*,16> getSyncVariablesInCall(CallSite &call) {
            SmallPtrSet<Function*,16> visitedFs;
            deque<Function*> fQueue;
            SmallPtrSet<Instruction*,16> callSites;
            SmallPtrSet<Value*,16> toReturn;
            Function* firstCall = getStripCall(&call);
            fQueue.push_back(firstCall);
            while (!fQueue.empty()) {
                Function* currFun = fQueue.front(); fQueue.pop_front();
                if (visitedFs.count(currFun) == 0) {
                    visitedFs.insert(currFun);
                    pair<SmallPtrSet<Instruction*,256>,
                         SmallPtrSet<Function*,16> > 
                        pairResult = getExecutableInsts(currFun);
                    for (auto it = pairResult.first.begin(),
                             et = pairResult.first.end();
                         it != et; ++it) {
                        if (isCallSite(*it)) {
                            callSites.insert(*it);
                            fQueue.insert(fQueue.end(),
                                          pairResult.second.begin(),
                                          pairResult.second.end());
                        }
                    }
                }
            }
            for (Instruction *callSite : callSites) {
                CallSite callS = CallSite(callSite);
                Function* calledFun = getStripCall(callSite);
                if (calledFun == NULL) {
                    //errs() << "Found call with non-easily resolvable function: " << *callSite << "\n";
                    continue;
                }
                //Basically, get the arguments from pthread calls, but nothing else
                if (calledFun->getName().equals("pthread_create")) {
                    //no sync arguments
                    ;
                } else if (calledFun->getName().equals("pthread_join")){
                    //no sync arguments
                    ;
                } else if ((calledFun->getName().startswith("pthread_") ||
                           calledFun->getName().startswith("sem_")) &&
                           !calledFun->getName().endswith("_init")) {                        
                    //Pthread calls have all arguments as syncs
                    //for (int i = 0; i < callS.getNumArgOperands(); ++i) {
                    if (callS.getNumArgOperands() > 0) {
                        Value* potentialSyncVar = callS.getArgOperand(0);
                        if (PointerType::classof(potentialSyncVar->getType())) {
                            toReturn.insert(potentialSyncVar);
                        }
                    }
                }
            }
            return toReturn;
        }

        void createIndexedCall(Function *toCall, Instruction* insertBef,int argument) {
            createDummyCall(toCall,insertBef,
                            ArrayRef<Value*>(ConstantInt::get(getGlobalContext(),APInt(32,argument))));
        }

        int bXDRFmI = 0;
        void createUniqueIndexCall(Function *toCall, Instruction* insertBef) {
            createDummyCall(toCall,insertBef,
                            ArrayRef<Value*>(ConstantInt::get(getGlobalContext(),APInt(32,bXDRFmI++))));
        }
        
        void createDummyCall(Function *toCall, Instruction* insertBef) {
            createDummyCall(toCall,insertBef,ArrayRef<Value*>());
        }
        
        //Creates a call to a function with a variable argument list, whose return value
        //cannot be used
        void createDummyCall(Function *toCall, Instruction* insertBef, ArrayRef<Value*> args) {
            CallInst* markCall = CallInst::Create(toCall,args);
            markCall->insertBefore(insertBef);
        }

        //Sets dummy to point to a function with name and a dumym implementation
        void createDummyFunction(Function *&dummy, string name, Module &M) {
            dummy =
                cast<Function>(M.getOrInsertFunction(name,
                                                     FunctionType::get(Type::getVoidTy(getGlobalContext()),
                                                                       true)));
            dummy->addFnAttr(Attribute::NoInline);
            dummy->addFnAttr(Attribute::OptimizeNone);
            BasicBlock *block = BasicBlock::Create(getGlobalContext(),
                                                   "entry", dummy);
            IRBuilder<true, NoFolder> builder(block);
            //Use a redundant add as a no-op
            builder.CreateBinOp(Instruction::Add,
                                ConstantInt::get(getGlobalContext(),APInt(32,1)),
                                ConstantInt::get(getGlobalContext(),APInt(32,1)));
            builder.CreateRetVoid();
        }

        //Creates the dummy nDRF and xDRF marker functions
        void setupDummyCalls(Function *&beginNDRFMark,
                             Function *&endNDRFMark,
                             Function *&endBarrierMark,
                             Function *&beginXDRFMark,
                             Function *&endXDRFMark,
                             Module &M) {
            createDummyFunction(beginNDRFMark,"begin_NDRF",M);
            createDummyFunction(endNDRFMark,"end_NDRF",M);
            createDummyFunction(endBarrierMark,"end_NDRF_BARRIER",M);
            createDummyFunction(beginXDRFMark,"begin_XDRF",M);
            createDummyFunction(endXDRFMark,"end_XDRF",M);
        }
        

        //Compares a potential synchronization variable towards the
        //ones already gathered, determines whether it
        //aliases. Creates a new synchronization variable
        //and adds it to the set if it does not alias.
        //Returns either the newly created SyncVar or a SyncVar that
        //the value aliases towards        
        SyncVar *checkSyncAlias(SmallPtrSet<SyncVar*,16> &syncVars, Value *toBeChecked) {
            SyncVar* addedTo = NULL;
            list<SyncVar*> aliasList;
            for (auto it = syncVars.begin(), eit = syncVars.end(); 
                 it != eit; ++it) {
                if ((*it)->checkAlias(toBeChecked,this))
                    aliasList.push_front(*it);
            }
            
            while (aliasList.size() > 1) {
                SyncVar* mergee = aliasList.back(); aliasList.pop_back();
                SyncVar* merger = aliasList.back();
                merger->merge(mergee);
                syncVars.erase(mergee);
                delete(mergee);
            }
            
            if (aliasList.size() == 0) {
                SyncVar* newSyncVar = new SyncVar;
#ifdef DEBUG_PRINT
                errs() << "Created new sync var: " << newSyncVar->sID << "\n";
#endif                //newSyncVar->rawSyncs.insert(toBeChecked);
                syncVars.insert(newSyncVar);
                aliasList.push_front(newSyncVar);
            }

            addedTo = aliasList.front();
            addedTo->rawSyncs.insert(toBeChecked);
            return addedTo;
        };
        
        void delimitRegions(Function *F, SmallPtrSet<NDRFRegion*,32> &regions,
                            SmallPtrSet<SyncVar*,16> &syncVars) {
            

            //Region *firstRegion = NULL;

            //type describes if a region should be DRF or nDRF
            enum RegionType {DRF, nDRF};
            
            typedef struct RegionDesc {
                RegionType type;
                BasicBlock* start;
            } RegionDesc;

            //Queue containing the new regions to start
            queue<RegionDesc*> regionQueue;
            //Queue containing the new basicblocks to examine
            //together with the block we examined when we added
            //them to the queue
            stack<tuple<BasicBlock*,BasicBlock*,int> > blockQueue;
            //Map mapping basicblocks to regions
            map<BasicBlock*,NDRFRegion*> blockRegionMap;
            
            SmallPtrSet<BasicBlock*, 128> visited;

            RegionDesc *firstDesc =  new RegionDesc;
            firstDesc -> type = DRF;
            firstDesc -> start = &(F->front());

            regionQueue.push(firstDesc);

            while (!regionQueue.empty()) {
                errs() << "R";
                errs().flush();
                RegionDesc *nextRegion = regionQueue.front(); 
                regionQueue.pop();
                
                RegionType mode = nextRegion->type;
                int nestLevel = 0;
                BasicBlock *entryBlock = nextRegion->start;
                
                if (vectorContainsBB(visited,entryBlock)) {
                    delete(nextRegion);
                    continue;
                }

                NDRFRegion *newRegion = NULL;
                if (mode == nDRF) {
                    newRegion = new NDRFRegion;
                    regions.insert(newRegion);
                    newRegion->entryBlocks.insert(entryBlock);
                }
                //At this point, we're done with the descriptor
                delete(nextRegion);

                blockQueue.push(make_tuple(entryBlock,(BasicBlock*)NULL,0));
                //We'll have the check of not pushing visited
                //blocks when we add them, so we don't check that here
                while (!blockQueue.empty()) {
                    tuple<BasicBlock *,BasicBlock*, int > tbe = blockQueue.top();
                    blockQueue.pop();
                    BasicBlock* toBeExamined = get<0>(tbe);
                    BasicBlock* previous = get<1>(tbe);
                    nestLevel = get<2>(tbe);

                    if (vectorContainsBB(visited,toBeExamined)) {
                        if (mode == DRF)
                            continue;
                        else {
                            NDRFRegion* containingRegion = blockRegionMap.at(toBeExamined);
                            assert(containingRegion &&
                                   "Encountered already visited DRF region in nDRF region");
                            //Merge regions
                            if (containingRegion != newRegion) {
                                assert(regions.erase(containingRegion) &&
                                       "Region to merge with was not in region container");
                                newRegion->merge(containingRegion);
                                for (auto it = containingRegion->inBlocks.begin(),
                                         et = containingRegion->inBlocks.end();
                                     it != et; ++it)
                                    blockRegionMap[*it] = newRegion;
                                delete(containingRegion);
                            } else
                                continue;
                        }
                    }

                    //Default treatment of blocks is to add them to the
                    //current region being built
                    //IN = this block to be placed inside region
                    //OUT = this block is a new region, end previous region
                    enum ResultMode {IN, OUT};
                    ResultMode resMode = IN;
                    //Check if the block is a synch point
                    if (nameBlockEndsInString(toBeExamined,PT_LOCK_POST)) {
                        //If we're already in an nDRF region, update sync
                        //variables and increment lock count
                        if (mode == nDRF) {
                            assert(newRegion && "In nDRF mode without a region");
                            assert("Negative nesting level" && nestLevel >= 0);
                            nestLevel += 1;
                            assert("First instruction is not a call" && CallInst::classof(&(toBeExamined->front())));
                            //CallInst *call = cast<CallInst>(&(toBeExamined->front()));
                            Instruction *inst = &(toBeExamined->front());
                            //Create sync variable object instead
                            bool newSyncVar = false;
                            Function* calledFun = getStripCall(inst);
                            CallSite call(inst);
                            SmallPtrSet<Value*,16> syncvars;
                            if (calledFun == NULL) {
                                //Could not determine called fun, make it non-enclave
                                //if (!OptimisticLooseNDRF)
                                newRegion->speccforcedNXDRF = true;
                            }
                            else if (calledFun->getName().equals("pthread_create")) {
                                //Do nothing
                                ;
                            } else if (calledFun->getName().equals("pthread_join")){
                                //Do nothing
                                ;
                            } else {
                                CallSite callS(call);
                                //Treat pthread calls and non-inlined calls differently.
                                //Pthread calls have as sync variables all their arguments
                                //We scan non-library calls to find the
                                if (!calledFun->empty() && !calledFun->getName().startswith("pthread_")) {
                                    SmallPtrSet<Value*,16> subSyncVars = getSyncVariablesInCall(callS);
                                    if (blackList.count(calledFun->getName()) != 0) {
                                        for (Use &arg : callS.args()) {
                                            Value *potentialFun = arg.get()->stripInBoundsConstantOffsets();
                                            if (auto fun = dyn_cast<Function>(potentialFun)) {
                                                SmallPtrSet<Value*,16> subArgSyncVars = getSyncVariablesInCall(callS);
                                                subSyncVars.insert(subArgSyncVars.begin(),subArgSyncVars.end());
                                            }
                                        }
                                    }
                                    syncvars.insert(subSyncVars.begin(),subSyncVars.end());
                                } else {
                                    if (calledFun->getName().equals("sem_wait")) {
                                        newRegion->hasWait = true;
                                        newRegion->isSemaphore = true;
                                    }
                                    if (calledFun->getName().equals("sem_post")) {
                                        newRegion->hasSignal = true;
                                        newRegion->isSemaphore = true;
                                    }   
                                    if (calledFun->getName().startswith("pthread_")) {
                                        if (calledFun->getName().equals("pthread_cond_wait"))
                                            newRegion->hasWait = true;
                                        if (calledFun->getName().equals("pthread_cond_signal"))
                                            newRegion->hasSignal = true;
                                        if (calledFun->getName().equals("pthread_cond_broadcast"))
                                            newRegion->hasSignal = true;
                                        if (call.getNumArgOperands() > 0 ) {
                                            Value* potentialSyncVar = call.getArgOperand(0);
                                            syncvars.insert(potentialSyncVar);
                                        }
                                    } else {
                                        for (int i = 0; i < call.getNumArgOperands(); ++i) {
                                            Value* potentialSyncVar = call.getArgOperand(i);
                                            syncvars.insert(potentialSyncVar);
                                        }
                                    }
                                }
                            }
                            for (Value* val : syncvars) {
                                Value* potentialSyncVar = val;
                                if (PointerType::classof(potentialSyncVar->getType())) {
                                    //#Tired of getting functions as arguments
                                    if (Function::classof(potentialSyncVar))
                                        continue;
                                    newSyncVar = true;
                                    SyncVar* insertedInto = checkSyncAlias(syncVars,potentialSyncVar);
                                    assert(insertedInto && "checkSyncAlias returned null!");
                                    newRegion->syncVars.insert(insertedInto);
                                    insertedInto->syncRegions.insert(newRegion);
                                }
                            }
// #ifdef DEBUG_PRINT_SOFT
//                             if (!newSyncVar) {
//                                 errs() << "The call : " << *call << "\n" << "did not have any pointer arguments\n";
//                             }
// #endif
                        } else {
                            resMode = OUT;
                        }
                    }
                    else if (nameBlockEndsInString(toBeExamined,PT_UNLOCK_POST)) {
                        if (mode == nDRF) {
                            nestLevel -= 1;
                            assert("Negative nesting level" && nestLevel >= 0);
                            if (nestLevel == 0) {
                                resMode = OUT;
                                newRegion->exitBlocks.insert(previous);
                            }
                        }
                    }

                    //This block begins a new region, do not mark it as visited
                    //yet
                    if (resMode == OUT) {
                        RegionDesc* newRegionDesc = new RegionDesc;
                        newRegionDesc->start = toBeExamined;
                        newRegionDesc->type = mode ==
                            DRF ? nDRF : DRF;
                        regionQueue.push(newRegionDesc);
                    } else {
                        //This block is now inserted into this region, mark it as
                        //visited
                        visited.insert(toBeExamined);
                        blockRegionMap[toBeExamined] = newRegion;
                        if (newRegion)
                            newRegion->inBlocks.insert(toBeExamined);
                        for (succ_iterator it = succ_begin(toBeExamined);
                             it != succ_end(toBeExamined); ++it) {
                            //Each succesor may be in this region,
                            //examine them further
                            blockQueue.push(make_tuple(*it,toBeExamined,nestLevel));
                        }
                    }
                }
            }
        }

        void insertDummySyncVars(SmallPtrSet<NDRFRegion*,32> &regions,
                            SmallPtrSet<SyncVar*,16> &syncVars) {
            for (auto it = regions.begin(), et = regions.end();
                 it != et; ++it) {
                if ((*it)->syncVars.size() == 0) {
                    SyncVar *dummyVar = new SyncVar();
                    (*it)->syncVars.insert(dummyVar);
                    dummyVar->syncRegions.insert(*it);
                    syncVars.insert(dummyVar);
                }
            }
        }

        //Checks which NDRF regions are likely to be barriers
        void checkForBarriers(SmallPtrSet<NDRFRegion*,32> regions) {
            for (NDRFRegion* reg : regions) {
                handleSignalWait(reg);
            }
        }


        //Checks whether is is likely that an NDRF region is a barrier
        void handleSignalWait(NDRFRegion* reg) {
            BasicBlock* entryBlock = *(reg->entryBlocks.begin());
            for (BasicBlock* block : reg->inBlocks) {
                Function *calledFun = getStripCall(&*(block->begin()));
                if (calledFun && calledFun->getName().equals("pthread_create")) {
                    reg->forcedNXDRF = true;
                    return;
                }
            }
            if (getStripCall(&*(entryBlock->begin())) &&
                getStripCall(&*(entryBlock->begin()))->getName().equals("pthread_join")) {
                for (SyncVar* sync : reg->syncVars)
                    sync->isBarrier = true;
                reg->isBarrier = true;
                return;
            }

            if (reg->hasSignal) {
                if (reg->hasWait) {
                    for (SyncVar* sync : reg->syncVars)
                        sync->isBarrier = true;
                    reg->isBarrier = true;
                }//  else {
                //     reg->forcedNXDRF = true;
                // }
            } // else
              //   if (reg->hasWait) {
              //       for (SyncVar* sync : reg->syncVars)
              //           sync->syncRegions.erase(reg);
              //       reg->syncVars.clear();
              //   }
            //Barriers must:
            //Have one entry
            // if (reg->entryBlocks.size() > 1)
            //     return false;
            // //Have one exit
            // if (reg->exitBlocks.size() > 1)
            //     return false;

            //Special cases: pthread_create and pthread_join are always
            //barriers

            // //Have a branch after the initial block
            // if (distance(succ_begin(entryBlock),succ_end(entryBlock))!=2)
            //     return false;
            // bool foundsignal = false, foundwait = false;
            // for (auto it = succ_begin(entryBlock),
            //          et = succ_end(entryBlock);
            //      it != et; ++it) {
            //     BasicBlock *singleSucc = (*it)->getSingleSuccessor();
            //     while (singleSucc && singleSucc != *(reg->exitBlocks.begin())) {
            //         Instruction &firstInst = singleSucc->front();
            //         if (isCallSite(&firstInst)) {
            //             CallSite call = CallSite(&firstInst);
            //             Function *fun = getStripCall(&call);
            //             if (fun->getName().equals("pthread_cond_wait")) {
            //                 foundwait = true;
            //                 break;
            //             }
            //             if (fun->getName().equals("pthread_cond_broadcast")) {
            //                 foundsignal = true;
            //                 break;
            //             }
            //         }
            //         singleSucc = singleSucc->getSingleSuccessor();
            //     }
            // }
            // return foundsignal && foundwait;
            //return false;
        }
        
        void findPaths(SmallPtrSet<NDRFRegion*,32> regions) {
            //First find the mapping of each block to their corresponding
            //region
            
            map<BasicBlock*,NDRFRegion*> regionOf;

            for (SmallPtrSet<NDRFRegion*,128>::iterator
                     begin = regions.begin(), end=regions.end();
                 begin != end; ++begin) {
                for (SmallPtrSet<BasicBlock*,64>::iterator
                         bit = (*begin)->inBlocks.begin(),
                         bend = (*begin)->inBlocks.end();
                     bit != bend; ++bit) {
                    regionOf[*bit] = *begin;
                }
            }
            
            //Now walk upwards or downwards from each nDRF region untill
            //we encounter an nDRF region or we have found all basicblocks
            for (SmallPtrSet<NDRFRegion*,128>::iterator
                     begin = regions.begin(), end=regions.end();
                 begin != end; ++begin) {
                if ((*begin)->isBarrier) {
                    continue;
                }
                SmallPtrSet<Instruction*,128> *pIs =
                    &((*begin)->precedingInsts);
                SmallPtrSet<Instruction*,128> *fIs =
                    &((*begin)->followingInsts);
                SmallPtrSet<NDRFRegion*,8> *pRs =
                    &((*begin)->precedingNDRFRegions);
                SmallPtrSet<NDRFRegion*,8> *fRs =
                    &((*begin)->followingNDRFRegions);
                deque<BasicBlock*> dfsQue;
                
                //Preceding and following functions are the functions that
                //are called before or after the nDRF region
                deque<Function*> pFs;
                deque<Function*> fFs;
                
                //Walk upwards
                for (auto start = (*begin)->entryBlocks.begin(),
                         end = (*begin)->entryBlocks.end();
                     start != end; ++start) {
                    dfsQue.insert(dfsQue.begin(),pred_begin(*start),pred_end(*start));
                    while (!dfsQue.empty()) {
                        BasicBlock* currBlock = dfsQue.front();
                        dfsQue.pop_front();
                        //Check if newly examined block is in an nDRF region
                        if (regionOf.count(currBlock) != 0) {
                            pRs->insert(regionOf[currBlock]);
                        } else {
                            bool hasInserted = false;
                            for (auto it = currBlock->begin(),
                                     et = currBlock->end();
                                 it != et; ++it) {
                                hasInserted = pIs->insert(&*it).second || hasInserted;
                                if (isa<CallInst>(*it)) {
                                    CallInst *callInst = &(cast<CallInst>(*it));
                                    Function *calledFun = callInst->getCalledFunction();
                                    if (calledFun && !calledFun->empty() && targetFunctions.count(calledFun) == 0)
                                        pFs.push_back(calledFun);
                                }
                            }
                            if (hasInserted)
                                dfsQue.insert(dfsQue.begin(),
                                              pred_begin(currBlock),
                                              pred_end(currBlock));
                        }
                    }
                }

                SmallPtrSet<Function*,16> pVisitedFs;
                while (!pFs.empty()) {
                    Function* currFun = pFs.front(); pFs.pop_front();
                    if (pVisitedFs.count(currFun) == 0) {
                        pVisitedFs.insert(currFun);
                        pair<SmallPtrSet<Instruction*,256>,
                             SmallPtrSet<Function*,16> > 
                            pairResult = getExecutableInsts(currFun);
                        pIs->insert(pairResult.first.begin(),
                                    pairResult.first.end());
                        pFs.insert(pFs.end(),
                                   pairResult.second.begin(),
                                   pairResult.second.end());
                    }
                }
                
                //Walk downwards
                for (auto start = (*begin)->exitBlocks.begin(),
                         end = (*begin)->exitBlocks.end();
                     start != end; ++start) {
                    dfsQue.insert(dfsQue.begin(),succ_begin(*start),succ_end(*start));
                    while (!dfsQue.empty()) {
                        BasicBlock* currBlock = dfsQue.front();
                        dfsQue.pop_front();
                        //Check if newly examined block is in an nDRF region
                        if (regionOf.count(currBlock) != 0) {
                            fRs->insert(regionOf[currBlock]); 
                        } else {
                            bool hasInserted = false;
                            for (auto it = currBlock->begin(),
                                     et = currBlock->end();
                                 it != et; ++it) {
                                hasInserted = fIs->insert(&*it).second || hasInserted;
                                if (isa<CallInst>(*it)) {
                                    CallInst *callInst = &(cast<CallInst>(*it));
                                    Function *calledFun = callInst->getCalledFunction();
                                    if (calledFun && !calledFun->empty() && targetFunctions.count(calledFun) == 0)
                                        fFs.push_back(calledFun);
                                }
                            }
                            if (hasInserted) {
                                dfsQue.insert(dfsQue.begin(),
                                              succ_begin(currBlock),
                                              succ_end(currBlock));
                            }                               
                        }
                    }
                }

                SmallPtrSet<Function*,16> fVisitedFs;
                while (!fFs.empty()) {
                    Function* currFun = fFs.front(); fFs.pop_front();
                    if (fVisitedFs.count(currFun) == 0) {
                        fVisitedFs.insert(currFun);
                        pair<SmallPtrSet<Instruction*,256>,
                             SmallPtrSet<Function*,16> > 
                            pairResult = getExecutableInsts(currFun);
                        fIs->insert(pairResult.first.begin(),
                                    pairResult.first.end());
                        fFs.insert(fFs.end(),
                                   pairResult.second.begin(),
                                   pairResult.second.end());
                    }
                }
            }
        }

        //This function analyzes each synchronization variable to
        //determine wether we can extend xDRF over it.
        void extendSynchronizations(SmallPtrSet<SyncVar*,16> syncvars) {
            for (auto it = syncvars.begin(), et = syncvars.end() ;
                 it != et; ++it) {
                if ((*it)->isBarrier)
                    continue;
                //pRegions is the set of regions whose preceding blocks
                //should be examined, correspondingly fRegions
                //is for the following blocks
                SmallPtrSet<NDRFRegion*,64> pRegions;
                SmallPtrSet<NDRFRegion*,64> fRegions;
                deque<NDRFRegion*> pQueue;
                deque<NDRFRegion*> fQueue;
                for (NDRFRegion *ptr : (*it)->syncRegions) {
                    if (ptr->shouldAnalyze()) {
                        if (!(ptr->hasWait && SignalWaitSane))
                            pQueue.push_back(ptr);
                        if (!(ptr->hasSignal && !ptr->isSemaphore && SignalWaitSane))
                            fQueue.push_back(ptr);
                    }
                }

                // pQueue.insert(pQueue.begin(),
                //               (*it)->syncRegions.begin(),
                //               (*it)->syncRegions.end());
                // fQueue.insert(fQueue.begin(),
                //               (*it)->syncRegions.begin(),
                //               (*it)->syncRegions.end());

                //Gather all nDRF regions whose predecessor blocks must be
                //examined. It is given that all nDRF regions associated
                //with a given synch variable are added at the same time
                //(intially all nDRFs associated with the current synch variable)
                //and that any region in the queue is enclave
                //There may be multiple copies of an nDRF region in the queue,
                //but their predeccessors will only be added once
                while (!pQueue.empty()) {
                    NDRFRegion *currRegion = pQueue.front();
                    pQueue.pop_front();
                    //If we have not handled this region before...
                    if (pRegions.insert(currRegion).second) {
                        //Find all its preceding nDRF regions
                        for (auto rit = currRegion->precedingNDRFRegions.begin(),
                                 ret = currRegion->precedingNDRFRegions.end();
                             rit != ret; ++rit) {
                            //If they are enclave
                            if ((*rit)->isEnclave()) {
                                if (!((*rit)->hasSignal && SignalWaitSane)) {
                                    //For each synch variable they use
                                    for (auto sit = (*rit)->syncVars.begin(),
                                             set = (*rit)->syncVars.end();
                                         sit != set; ++sit) {
                                        //Add all nDRF regions using this variable
                                        //to the search
                                        pQueue.insert(pQueue.begin(),
                                                      (*sit)->syncRegions.begin(),
                                                      (*sit)->syncRegions.end());
                                    }
                                } else {
                                    pQueue.push_front(*rit);
                                }
                            }
                        }
                    }
                }
                //Same thing, only for following regions
                while (!fQueue.empty()) {
                    NDRFRegion *currRegion = fQueue.front();
                    fQueue.pop_front();
                    if (fRegions.insert(currRegion).second) {
                        for (auto rit = currRegion->followingNDRFRegions.begin(),
                                 ret = currRegion->followingNDRFRegions.end();
                             rit != ret; ++rit) {
                            if ((*rit)->isEnclave()) {
                                if (!((*rit)->hasWait && SignalWaitSane)) {
                                    for (auto sit = (*rit)->syncVars.begin(),
                                             set = (*rit)->syncVars.end();
                                         sit != set; ++sit) {
                                        fQueue.insert(fQueue.begin(),
                                                      (*sit)->syncRegions.begin(),
                                                      (*sit)->syncRegions.end());
                                    }
                                } else {
                                    fQueue.push_front(*rit);
                                }
                            }
                        }
                    }
                }
                //Gather up basic blocks for comparison
                SmallPtrSet<Instruction*,256> predInsts;
                SmallPtrSet<Instruction*,256> folwInsts;
                for (auto rit = pRegions.begin(), ret = pRegions.end();
                     rit != ret; ++rit) {
                    for (auto iit = (*rit)->precedingInsts.begin(),
                             eit = (*rit)->precedingInsts.end();
                         iit != eit; ++iit) {
                        if (StoreInst::classof(*iit)
                            //|| isCallSite(currInst) //Assume library calls cannot cause conflicts
                            ) {
                            predInsts.insert(*iit);
                        }
                        // predInsts.insert((*rit)->precedingInsts.begin(),
                        // (*rit)->precedingInsts.end());
                    }
                }
                for (auto rit = fRegions.begin(), ret = fRegions.end();
                     rit != ret; ++rit) {
                    for (auto iit = (*rit)->followingInsts.begin(),
                             eit = (*rit)->followingInsts.end();
                         iit != eit; ++iit) {
                        if (StoreInst::classof(*iit) ||
                            LoadInst::classof(*iit)
                            //|| isCallSite(currInst) //Assume library calls cannot cause conflicts
                            ) {
                            folwInsts.insert(*iit);
                        }
                        // folwInsts.insert((*rit)->followingInsts.begin(),
                        //                  (*rit)->followingInsts.end());
                    }
                }
                //Finally, we cross-check the blocks
#ifdef DEBUG_PRINT_SOFT
                errs() << "Cross checking for syncvar ID: " << (*it)->sID << "\n"; 
#endif
                errs() << "Cross sizes are: " << predInsts.size() << "x" << folwInsts.size() << "\n";
                if (crossCheckInstCollections(predInsts,folwInsts)) {
                    (*it)->enclave = true;
                }
            }
        }
        
        //Makes sure that no syncVar is enclave if it is used by a
        //non-enclave nDRF region
        void contractSyncVarEnclave(SmallPtrSet<NDRFRegion*,128> regions) {
            deque<NDRFRegion*> toBeChecked(regions.begin(),regions.end());
            while (!toBeChecked.empty()) {
                NDRFRegion *next = toBeChecked.front(); toBeChecked.pop_front();
                if (!next->isEnclave()) {
                    for (SyncVar* svar : next->syncVars) {
                        if (svar->enclave) {
                            svar->enclave = false;
                            toBeChecked.insert(toBeChecked.end(),
                                               svar->syncRegions.begin(),
                                               svar->syncRegions.end());
                        }
                    }
                }
            }
        }
        
                

        //This functions verifies that no write in the first set
        //of basicblocks conflicts with a read or write in the second set
        //It returns true if it is the case that no conflict has occured
        bool crossCheckInstCollections(SmallPtrSet<Instruction*,256> &pStores,
                                        SmallPtrSet<Instruction*,256> &fLoadsStores) {

            bool toreturn = true;
            for (auto pt = pStores.begin(), pet = pStores.end();
                 pt != pet && (toreturn || FullyCheck)
                     ; ++pt) {
                for (auto ft = fLoadsStores.begin(), fet = fLoadsStores.end();
                     ft != fet && (toreturn || FullyCheck)
                         ; ++ft) {
                    visitedAliasValues.clear();
                    if (pointerConflict(cast<Instruction>(*pt),cast<Instruction>(*ft))) {
                        if (!FullyCheck) {
                            errs() << "These instructions conflicted:\n";
                            errs() << **pt << " : " << typeid(**pt).name() <<"\n";
                            errs() << **ft << " : " << typeid(**ft).name() << "\n";
                        }
                        toreturn = false;
                    } 
                }
            }
#ifdef DEBUG_PRINT_SOFT
            if (!toreturn) {
                errs() << "Found a conflict somewhere between " << pStores.size()
                       << " stores and " << fLoadsStores.size() << " loads or stores\n";
            } else {
                errs() << "Found no conflict between " << pStores.size()
                   << " stores and " << fLoadsStores.size() << " loads or stores\n";
            }
#endif
            return toreturn;
        }
        
        void printSyncVarInfo(SmallPtrSet<SyncVar*,16> &syncvars) {
#ifdef DEBUG_PRINT_SOFT
            for (auto it = syncvars.begin(); it != syncvars.end(); ++it) {
                errs() << "Synchronization value group : " << (*it)->sID << "\n";
                // if (MultiVersioning) {
                //     for (int i = 0; i < NUM_VERSIONS; ++i)
                //         errs() << "  is " << ((*it)->enclave_version[i] ? "enclave" : "not enclave") << " in version: " << i << "\n";
                // } else {
                    errs() << "  is " << ((*it)->enclave ? "enclave" : "not enclave") << "\n";
                // }
                errs() << "  Contains synchronization values:\n";
                //for (auto vit = (*it)->rawSyncs.begin();;){
                    //vit != (*it)->rawSyncs.end(); ++vit) {
                    //errs() << "    " << **vit << "\n";
                    //}
                if ((*it)->rawSyncs.size() > 0)
                    errs() << "    " << **((*it)->rawSyncs.begin()) << "\n";
                errs() << "  Used by nDRF regions: ";
                for (auto rit = (*it)->syncRegions.begin();
                     rit != (*it)->syncRegions.end(); ++rit) {
                    errs() << (*rit)->rID << ", ";
                }
                errs() << "\n";
            }
#endif
        }

        void printRegionInfo(SmallPtrSet<NDRFRegion*,128> &regions) {
#ifdef DEBUG_PRINT_SOFT
            for (auto it = regions.begin(), et = regions.end();
                 it != et; ++it) {
                errs() << "NDRF Region : " << (*it)->rID << "\n";
                if ((*it)->isBarrier)
                    errs() << "  is barrier\n";
                if ((*it)->forcedNXDRF)
                    errs() << "  is forced nXDRF\n";
                if ((*it)->isEnclave())
                    errs() << "  is enclave\n";
                else
                    errs() << "  is not enclave\n";
                errs() << "  Uses sync vars : \n";
                for (auto sit = (*it)->syncVars.begin();
                     sit != (*it)->syncVars.end(); ++sit) {
                    errs() << "    " << (*sit)->sID << "\n";
                }

                errs() << "  Contains blocks : \n";
                for (auto bit = (*it)->inBlocks.begin();
                     bit != (*it)->inBlocks.end(); ++bit) {
                    errs() << "    " << (*bit)->getName() << "\n";
                }
#ifdef DEBUG_PRINT
                errs() << "  Is preceded by instructions : \n";
                for (auto bit = (*it)->precedingInsts.begin();
                     bit != (*it)->precedingInsts.end(); ++bit) {
                    if (StoreInst::classof(*bit) ||
                        LoadInst::classof(*bit)) {
                        errs() << "    " << *(*bit) << "\n";
                    }
                }
                errs() << "  Is followed by instructions : \n";
                for (auto bit = (*it)->followingInsts.begin();
                     bit != (*it)->followingInsts.end(); ++bit) {
                    if (StoreInst::classof(*bit) ||
                        LoadInst::classof(*bit)) {
                        errs() << "    " << *(*bit) << "\n";
                    }
                }
                    
                errs() << "  Preceding nDRFs are: ";
                for (auto bit = (*it)->precedingNDRFRegions.begin(),
                         beit = (*it)->precedingNDRFRegions.end() ;
                     bit != beit; ++bit) {
                    errs() << (*bit)->rID << ", ";
                }
                errs() << "\n";
                
                errs() << "  Following nDRFs are: ";
                for (auto bit = (*it)->followingNDRFRegions.begin(),
                         beit = (*it)->followingNDRFRegions.end() ;
                     bit != beit; ++bit) {
                    errs() << (*bit)->rID << ", ";
                }
                errs() << "\n";
#endif                
            }
#endif
        }



        void markRegions(Function *beginNDRFMark,
                         Function *endNDRFMark,
                         Function *endBarrierMark,
                         Function *beginXDRFMark,
                         Function *endXDRFMark,
                         SmallPtrSet<NDRFRegion*,128> &regions) {
            for (SmallPtrSet<NDRFRegion*,128>::iterator 
                     it = regions.begin(), et = regions.end();
                 it != et; ++it) {
                // errs() << "M";
                // errs().flush();
                vector<Value*> arglist;
                // for (auto st = (*it)->syncVars.begin(),
                //          et = (*it)->syncVars.end();
                //      st != et; ++st)
                //     arglist.push_back(ConstantInt::get(getGlobalContext(),APInt(32,(*st)->sID)));
                arglist.push_back(ConstantInt::get(getGlobalContext(),APInt(32,(*it)->rID)));
                ArrayRef<Value*> args(arglist);
                for (SmallPtrSet<BasicBlock*,8>::iterator
                         entryit = (*it)->entryBlocks.begin(),
                         entryet = (*it)->entryBlocks.end();
                     entryit != entryet; ++entryit) {
                    createDummyCall(beginNDRFMark,&((*entryit)->front()),args);
                    if (MultiVersioning) {
                        for (int i = 0; i < NUM_VERSIONS; ++i) {
                            if (!((*it)->isEnclaveVer(i)))
                                createIndexedCall(endXDRFMark,&((*entryit)->front()),i);
                        }
                    }
                    else {
                        if (!((*it)->isEnclave())) {
                            createDummyCall(endXDRFMark,&((*entryit)->front()));
                        }
                    }
                }
                for (SmallPtrSet<BasicBlock*,8>::iterator
                         exitit = (*it)->exitBlocks.begin(),
                         exitet = (*it)->exitBlocks.end();
                     exitit != exitet; ++exitit) {
                    if ((*it)->isBarrier)
                        createDummyCall(endBarrierMark,&((*exitit)->back()),args);
                    else
                        createDummyCall(endNDRFMark,&((*exitit)->back()),args);
                    if (MultiVersioning) {
                        for (int i = 0; i < NUM_VERSIONS; ++i) {
                            if (!((*it)->isEnclaveVer(i)))
                                createIndexedCall(beginXDRFMark,&((*exitit)->back()),i);
                        }
                    }
                    else if (!((*it)->isEnclave()))
                        createDummyCall(beginXDRFMark,&((*exitit)->back()));
                }
            }
            errs() << "\n";
        }
        
        bool nameBlockEndsInString(BasicBlock* BB, string str){
            return ((BB->getName().str().rfind(str) ==
                     BB->getName().str().size()-str.size()) &&
                    (BB->getName().str().rfind(str) != string::npos));
        }
        
        
        //Attempts to resolve a value to an integer using scalar evolution
        //Is only more powerfull than constant folding in loops, where it will
        //reduce an iterative variable to its base value
        bool isBaseConstantInLoopSCEV(Value *val, ConstantInt **base, Function *containingFun) {
            ScalarEvolution *SE = getSE(containingFun);
            const SCEV* scev_val = SE->getSCEV(val);
            // errs() << "Resolving SCEV for val: " << *val << " - ";
            // scev_val->print(errs());
            // errs() << "\n";
            while (!isa<SCEVConstant>(scev_val)) {
                if (auto scev_rec = dyn_cast<SCEVAddRecExpr>(scev_val)) {
                   scev_val = scev_rec->getStart();
                   continue;
                }
                // errs() << "Resolution failed, stopped at: ";
                // scev_val->print(errs());
                // errs() << "\n";
                return false;
            }
            *base = dyn_cast<SCEVConstant>(scev_val)->getValue();
            
            // if (scev_val != SE->getSCEV(val)) {
            //     errs() << "Resolved non-constant to: " << **base << "\n";
            // }
            return true;
        }

        //Returns wether the dynamic gep was resolved
        bool handleDynamicGEP(Value **pt_,APInt &offset) {
            //errs() << "!Detected dynamic use of GEP: " << **pt_ << ", #yolo-ing it\n";                    
            //Basically, we will try to get a constantExpr out of
            //the index and then manually recalculate our offset
            //and set the stripped value to it's pointer
            int offsetint = 0;
            Value *pt = *pt_;
            Function *containingFun = dyn_cast<Function>(dyn_cast<Instruction>(*pt_)->getParent()->getParent());
            Type* nextType = dyn_cast<CompositeType>(dyn_cast<GetElementPtrInst>(pt)->getPointerOperand()->getType());
            for (auto it = dyn_cast<GetElementPtrInst>(pt)->idx_begin(),
                     et = dyn_cast<GetElementPtrInst>(pt)->idx_end();
                 it != et; ++it) {
                assert(isa<CompositeType>(nextType) && "Broken Dynamic GEP detected");
                CompositeType *currType = cast<CompositeType>(nextType);
                ConstantInt *constant = NULL;
                bool isConstant = isBaseConstantInLoopSCEV(*it, &constant,containingFun);
                if (!isConstant) {
                    offsetint = -1;
                    break;
                }
                int curroffset = constant->getZExtValue();
                nextType = currType->getTypeAtIndex(curroffset);

                // Handle a struct index, which adds its field offset to the pointer.
                if (StructType *STy = dyn_cast<StructType>(currType)) {
                    const StructLayout *SL = wM->getDataLayout().getStructLayout(STy);
                    offsetint += SL->getElementOffset(curroffset);
                } else {
                    // For array or vector indices, scale the index by the size of the type.
                    offsetint += curroffset *
                        wM->getDataLayout().getTypeAllocSize(nextType);
                }
            }
            *pt_ = cast<GetElementPtrInst>(pt)->getPointerOperand();
            if (offsetint >= 0) {
                //errs() << "Resolved Dynamic GEP" << "\n";
                offset += APInt(offset.getBitWidth(),offsetint);
                return true;
            } else {
                //offset += APInt(offset.getBitWidth(),0);
                return false;
                //errs() << "YOLO-ed Dynamic GEP" << "\n";
                //*pt_ = NULL;
            }
        }

        SmallPtrSet<Value*,1024> visitedBottomLevelValues;
        
        set<pair<Value*,list<int> > > findBottomLevelValues(Value *val) {
            visitedBottomLevelValues.clear();
            return findBottomLevelValues_(val);
        }
        
        set<pair<Value*,list<int> > > findBottomLevelValues_(Value *val) {
            int pointerSize = wM->getDataLayout().getPointerSizeInBits(cast<PointerType>(val->getType())->getAddressSpace());
            APInt offset = APInt(pointerSize,0);
            Value* strip = val->stripAndAccumulateInBoundsConstantOffsets(wM->getDataLayout(),offset);
            int intoffset = offset.getLimitedValue();
            set<pair<Value*,list<int> > > toReturn;
            bool appendOffset = true;
            SmallPtrSet<Value*,2> nextPointers;

            if (visitedBottomLevelValues.count(val) != 0)
                return toReturn;
            visitedBottomLevelValues.insert(val);
            
            //These are bottom level values, they modify toReturn directly
            //and do not add pointers to nextPointers
            if (auto inst = dyn_cast<Instruction>(strip)) {
                if (isCallSite(inst)) {
                    CallSite call(strip);
                    Function* calledFun = getStripCall(&call);
                    if (calledFun && (calledFun->getName().equals("malloc") ||
                                      calledFun->getName().equals("MyMalloc"))) {
                        if (escapeCheck(strip)) {
                            toReturn.insert(make_pair((Value*)NULL,list<int>(1,intoffset)));
                            //errs() << "Found escaped allocation: " << *inst << "\n";
                        }
                    }
                    else {
                        toReturn.insert(make_pair((Value*)NULL,list<int>(1,intoffset)));
                        //errs() << "Found callsite: " << *inst << "\n";
                    }
                }
            }
            if (auto glob = dyn_cast<GlobalVariable>(strip)) {
                //errs() << "Found global: " << *glob << "\n";
                toReturn.insert(make_pair(glob,list<int>(1,intoffset)));
            }
            if (auto arg = dyn_cast<Argument>(strip)) {
                //errs() << "Found argument: " << *arg << "\n";
                toReturn.insert(make_pair((Value*)NULL,list<int>(1,intoffset)));
            }

            //These are the recursive cases, they add pointers to nextPointers
            //and define how to merge offsets
            if (auto load = dyn_cast<LoadInst>(strip)) {
                nextPointers.insert(load->getPointerOperand());
            }
            if (auto dyngep = dyn_cast<GetElementPtrInst>(strip)) {
                nextPointers.insert(dyngep->getPointerOperand());
                appendOffset = false;
                if (AssumeDGEPAliasConstBase) {
                    if (!handleDynamicGEP(&strip,offset)) {
                        if (!AssumeDGEPNoAlias) {
                            intoffset = -1;
                        } else
                            nextPointers.erase(dyngep->getPointerOperand());
                    } else {
                        intoffset = offset.getLimitedValue();
                    }
                } else {
                    if (!AssumeDGEPNoAlias) {
                        intoffset = -1;
                    } else
                        nextPointers.erase(dyngep->getPointerOperand());
                }
            }
            if (auto phi = dyn_cast<PHINode>(strip)) {
                appendOffset = false;
                for (Use &use : cast<PHINode>(strip)->incoming_values())
                    nextPointers.insert(use.get());
            }

#define UNIFY_OFFSETS(X,Y) ((X == -1 || Y == -1) ? -1 : X+Y)

            for (Value* nextPoint : nextPointers) {
                set<pair<Value*,list<int> > > hasReturn = findBottomLevelValues_(nextPoint);
                for (set<pair<Value*,list<int> > >::iterator retPair = hasReturn.begin(),
                         end_retPair = hasReturn.end(); retPair != end_retPair; ++retPair) {
                    list<int> newList = retPair->second;
                    if (appendOffset)
                        newList.push_back(intoffset);
                    else {
                        int oldlatest = newList.back();
                        newList.pop_back();
                        newList.push_back(UNIFY_OFFSETS(oldlatest,intoffset));
                    }
                    toReturn.insert(make_pair(retPair->first,newList));
                }
            }
#undef UNIFY_OFFSETS
        }
        
        map<pair<Value*,Value*>, bool> pointerAliasDynamic;

        int numDynamicFailures = 0;
        int numArgumentCallsiteFailures = 0;

        bool pointerAlias(Value *pt1, Value *pt2) {
            if (pointerAliasDynamic.count(make_pair(pt1,pt2)) != 0)
                return pointerAliasDynamic[make_pair(pt1,pt2)];

            set<pair<Value*,list<int> > > bottomUsesPt1 = findBottomLevelValues(pt1);
            set<pair<Value*,list<int> > > bottomUsesPt2 = findBottomLevelValues(pt2);

            bool toReturn = false;

            for (set<pair<Value*,list<int> > >::iterator pt1pair = bottomUsesPt1.begin(),
                     end_pt1pair = bottomUsesPt1.end();
                 pt1pair != end_pt1pair && (!toReturn || FullyCheck);++pt1pair) {
                for (set<pair<Value*,list<int> > >::iterator pt2pair = bottomUsesPt2.begin(),
                         end_pt2pair = bottomUsesPt2.end();
                     pt2pair != end_pt2pair && (!toReturn || FullyCheck);++pt2pair) {
                    if (!pt1pair->first || !pt2pair->first) {
                        numArgumentCallsiteFailures++;
                        //errs() << "Found conflict because of finding either an argument or a callsite\n";
                        toReturn = false;
                        continue;
                        //return pointerAliasDynamic[make_pair(pt1,pt2)]=true;
                    }
                    if (pt1pair->first != pt2pair->first)
                        continue;
                    auto pt1b = pt1pair->second.rbegin();
                    auto pt2b = pt2pair->second.rbegin();
                    bool listChecksOut = true;
                    while (pt1b != pt1pair->second.rend() &&
                           pt2b != pt2pair->second.rend()) {
                        if (*pt1b != -1 && *pt2b != -1 && *pt1b != *pt2b) {
                            listChecksOut = false;
                            break;
                        }
                        if (*pt1b == -1 || *pt2b == -1)
                            numDynamicFailures++;
                        pt1b++;
                        pt2b++;
                    }
                    if (pt1b != pt1pair->second.rend() ||
                        pt2b != pt2pair->second.rend())
                        continue;
                    if (listChecksOut) {
                        // errs() << "Found conflict between:\n";
                        // errs() << *(pt1pair->first);
                        // errs() << " : [";
                        // for (auto pt1b = pt1pair->second.begin(),
                        //          pt1e = pt1pair->second.end();
                        //      pt1b != pt1e; ++pt1b)
                        //     errs() << *pt1b << ", ";
                        // errs() << "]\nand\n";
                        // errs() << *(pt2pair->first);
                        // errs() << " : [";
                        // for (auto pt2b = pt2pair->second.begin(),
                        //          pt2e = pt2pair->second.end();
                        //      pt2b != pt2e; ++pt2b)
                        //     errs() << *pt2b << ", ";
                        // errs() << "]\n";
                        toReturn = true;
                        //return pointerAliasDynamic[make_pair(pt1,pt2)]=true;
                    }
                }
            }
            return pointerAliasDynamic[make_pair(pt1,pt2)]=toReturn;
        }

        SmallPtrSet<Value*,1024> visitedAliasValues;

        //Checks whether the address val might escape the local context
        bool escapeCheck(Value *val) {
            if (!isa<PointerType>(val->getType()))
                return false;
            if (isa<GlobalValue>(val))
                return true;
            bool toReturn = false;
            for (Use &use : val->uses()) {
                Value * useval = use.get();
                if (auto store = dyn_cast<StoreInst>(useval))
                    toReturn =  toReturn || escapeCheck(store->getPointerOperand());
                if (auto load = dyn_cast<LoadInst>(useval))
                    toReturn = toReturn || false;
                if (Instruction* useinst = dyn_cast<Instruction>(useval))
                    toReturn = toReturn || isCallSite(useinst);
                toReturn = toReturn || escapeCheck(val);
            }
            return toReturn;
        }


        //Pointer comparsion
        //Checks wether the pointer arguments to two instructions
        //do not alias
        bool pointerConflict(Instruction *P1, Instruction *P2) {
            SmallPtrSet<Value*,4> P1Pargs;
            SmallPtrSet<Value*,4> P2Pargs;
            if (auto P1_load = dyn_cast<LoadInst>(P1))
                P1Pargs.insert(P1_load->getPointerOperand());
            if (auto P1_store = dyn_cast<StoreInst>(P1))
                P1Pargs.insert(P1_store->getPointerOperand());
            if (auto P1_call = CallSite(P1))
                for (Use &arg : P1_call.args())
                    if (isa<PointerType>(arg.get()->getType()))
                        P1Pargs.insert(arg.get());
            if (auto P2_load = dyn_cast<LoadInst>(P2))
                P2Pargs.insert(P2_load->getPointerOperand());
            if (auto P2_store = dyn_cast<StoreInst>(P2))
                P2Pargs.insert(P2_store->getPointerOperand());
            if (auto P2_call = CallSite(P2))
                for (Use &arg : P2_call.args())
                    if (isa<PointerType>(arg.get()->getType()))
                        P2Pargs.insert(arg.get());
            bool toReturn = false;
            for (Value *P1a : P1Pargs) {
                if (toReturn && !FullyCheck)
                    break;
                if(isa<PointerType>(P1a->getType()))
                    for (Value *P2a : P2Pargs) {
                        if (toReturn && !FullyCheck)
                            break;
                        if (isa<PointerType>(P2a->getType()))
                            if (pointerAlias(P1a,P2a)) {
                                toReturn = true;
                            }
                    }
            }
            return toReturn;
        }

        //ModRef comparison
        //Check wether the store/load/call P2 accesses the location of the
        //store/load/call P1
        bool modRefConflict(Instruction *P1, Instruction *P2) {
            // if (P1->getParent()->getParent() != P2->getParent()->getParent()) {
            //     errs() << "!!!!!!Comparing insts from different functions" << "\n";
            // }
             bool toreturn = true;
            // if (isCallSite(P1)) {
            //     ImmutableCallSite P1_call = ImmutableCallSite(P1);
            //     toreturn = !(AA->getModRefInfo(P2,P1_call) == AliasAnalysis::NoModRef);
            // } else if (isCallSite(P2)) {
            //     ImmutableCallSite P2_call = ImmutableCallSite(P2);
            //     toreturn = !(AA->getModRefInfo(P1,P2_call) == AliasAnalysis::NoModRef);
            // } else if (AA->getModRefInfo(P2,MemoryLocation::get(P1)) == AliasAnalysis::NoModRef) {
            //     toreturn =  false;
            // }
            //Attempt to salvage poor alias analysis
            if (toreturn) {
                toreturn = pointerConflict(P1,P2);
            }
            return toreturn;
        }
        
        //Tries to fully strip a pointer of its type
        Type* stripPointer(Type *toStrip) {
            while (isa<PointerType>(toStrip)) {
                toStrip = cast<PointerType>(toStrip)->getPointerElementType();
            }
            return toStrip;
        }

        //Checks wether a value is a dynamic GEP
        Value* isDynamicGEP(Value *val) {
            Value* valstrip = val->stripInBoundsConstantOffsets();

            if (isa<GetElementPtrInst>(valstrip)) {
                return valstrip;
            }

            if (isa<LoadInst>(valstrip)) {
                return isDynamicGEP(cast<LoadInst>(valstrip)->getPointerOperand());
            }

            if (isa<StoreInst>(valstrip)) {
                return isDynamicGEP(cast<StoreInst>(valstrip)->getPointerOperand());
            }
            return NULL;
        }

        //attempt to differentiate between mutexes within the same structure
        //Should return the end-points on the use-chains, together with
        //the complete offset list towards that end-point
        // set<pair<Value*,list<int> > > offsetsToNextLoad(Value *val) {
        //     return offsetsToNextLoad_(val,visited_temp);
        // }

        map<Value*,set<pair<Value*,list<int> > > > offsetsToNextLoadDynamic;

        set<pair<Value*,list<int> > > offsetsToNextLoad_(Value *val,SmallPtrSet<Value*,1024> &visited) {
            //We cannot actually strip anything here, since we don't even want
            //to strip all-zero geps

            set<pair<Value*,list<int> > > toReturn;

            if (!visited.insert(val).second) {
                return toReturn;
            }

            if (offsetsToNextLoadDynamic.count(val) != 0)
                return offsetsToNextLoadDynamic[val];

            //Instead, we manually walk past casts
            Value * stripCasts = val;
            while (Operator::getOpcode(stripCasts) == Instruction::BitCast ||
                   Operator::getOpcode(stripCasts) == Instruction::AddrSpaceCast)
                stripCasts = cast<Operator>(stripCasts)->getOperand(0);

            if (!isa<PointerType>(stripCasts->getType()))
                return toReturn;

            //We have now walked past all casts, and can handle the
            //value correctly
            if (auto gep = dyn_cast<GetElementPtrInst>(stripCasts)) {
                //handle indices
                list<int> offsetList;
                for (auto it = gep->idx_begin(),
                         et = gep->idx_end(); it != et; it++) {
                    if (auto constInt = dyn_cast<ConstantInt>(*it))
                        offsetList.push_back(constInt->getZExtValue());
                    else
                        offsetList.push_back(-1);
                }
                set<pair<Value*,list<int> > > imReturn = offsetsToNextLoad_(gep->getPointerOperand(),visited);
                for (auto imb = imReturn.begin(), ime = imReturn.end();
                     imb != ime; ++imb) {
                    list<int> tempList = imb->second;
                    tempList.insert(tempList.end(),offsetList.begin(),offsetList.end());
                    toReturn.insert(make_pair(imb->first,
                                              tempList));
                }
            }
            if (auto arg = dyn_cast<Argument>(stripCasts)) {
                toReturn.insert(make_pair(arg,list<int>()));
            }
            if (auto glob = dyn_cast<GlobalVariable>(stripCasts)) {
                toReturn.insert(make_pair(glob,list<int>()));
            }
            if (auto load = dyn_cast<LoadInst>(stripCasts)) {
                toReturn.insert(make_pair(load,list<int>()));
            }
            if (auto phi = dyn_cast<PHINode>(stripCasts)) {
                //Do not walk through the same node twice

                for (Use &use : phi->incoming_values()) {
                    set<pair<Value*,list<int> > > imReturn = offsetsToNextLoad_(use.get(),visited);
                    toReturn.insert(imReturn.begin(),imReturn.end());
                }
            }
            return offsetsToNextLoadDynamic[val]=toReturn;
        }
        
        SmallPtrSet<Value*,1024> visitedMutexValues;
        set<pair<Value*,int> > possibleLoads(Value *val) {
            //visitedMutexValues.clear();
            return possibleLoads_(val);
        }

        set<pair<Value*,int> > possibleLoads_(Value *val) {
            int pointerSize = wM->getDataLayout().getPointerSizeInBits(cast<PointerType>(val->getType())->getAddressSpace());
            APInt offset = APInt(pointerSize,0);
            Value* strip = val->stripAndAccumulateInBoundsConstantOffsets(wM->getDataLayout(),offset);
            int offset_int = offset.getLimitedValue();
            set<pair<Value*,int> > possloads;

            // errs() << "Started load search with :\n";
            // errs() << val << " : " << typeid(*val).name() << "\n";

            if (auto load = dyn_cast<LoadInst>(strip)) {
                possloads.insert(make_pair(load,offset_int));
            }
            else if (auto glob = dyn_cast<GlobalVariable>(strip)) {
                possloads.insert(make_pair(glob,offset_int));
            }
            else if (auto null = dyn_cast<ConstantPointerNull>(strip)) {
            }
            else if (auto arg = dyn_cast<Argument>(strip)) {
                possloads.insert(make_pair(arg,offset_int));
            }
            else if (auto gep = dyn_cast<GetElementPtrInst>(strip)) {
                set<pair<Value*,int> > subpossloads = possibleLoads_(gep->getPointerOperand());
                for (set<pair<Value*,int> >::iterator lopair = subpossloads.begin(),
                         lopair_end = subpossloads.end(); lopair != lopair_end; ++lopair)
                    possloads.insert(make_pair(lopair->first,-1));
            }
            else if (auto phi = dyn_cast<PHINode>(strip)) {
                if (visitedMutexValues.count(phi) == 0) {
                    visitedMutexValues.insert(phi);
                    for (Use &use : phi->incoming_values()) {
                        set<pair<Value*,int> > subpossloads = possibleLoads_(use.get());
                        for (set<pair<Value*,int> >::iterator lopair = subpossloads.begin(),
                                 lopair_end = subpossloads.end(); lopair != lopair_end; ++lopair)
                            possloads.insert(make_pair(lopair->first,lopair->second+offset_int));
                    }
                }
            } else {
                throw strip;
                //assert(!"Fell through all use cases while tracing mutex use");
            }        
            return possloads;
        }

        map<pair<Value*,Value*>,bool> mutexAliasDynamic;

        bool mutexAlias_(Value *mut1,Value *mut2) {
            visited_temp1.clear();
            visited_temp2.clear();
            bool toReturn = mutexAlias(mut1,mut2);
            return mutexAliasDynamic[make_pair(mut1,mut2)]=toReturn;
        }

        SmallPtrSet<Value*,1024> visited_temp1;
        SmallPtrSet<Value*,1024> visited_temp2;

        //Checks wether two pointer-type values Mut1 and Mut2 potentially
        //point to the same mutex
        bool mutexAlias(Value *mut1, Value *mut2) {
            set<pair<Value*,list<int> > > offsetToNextLoadmut1;
            set<pair<Value*,list<int> > > offsetToNextLoadmut2;

            if (mutexAliasDynamic.count(make_pair(mut1,mut2)) != 0)
                return mutexAliasDynamic[make_pair(mut1,mut2)];

            offsetToNextLoadmut1 = offsetsToNextLoad_(mut1,visited_temp1);
            offsetToNextLoadmut2 = offsetsToNextLoad_(mut2,visited_temp2);
 
            // errs() << "Started mutex alias with :\n";
            // errs() << mut1 << "=" << mut1->getName() <<  " : " << typeid(*mut1).name() << "\n";
            // errs() << mut2 << "=" << mut2->getName() << " : " << typeid(*mut2).name() << "\n";

            for (auto  m1 = offsetToNextLoadmut1.begin(),
                     m1end = offsetToNextLoadmut1.end(); m1 != m1end; ++m1) {
                for (auto m2 = offsetToNextLoadmut2.begin(),
                         m2end = offsetToNextLoadmut2.end(); m2 != m2end; ++m2) {
                    //If we can determine that the targeted origins are different
                    //there is no alias

                    //If both are arguments, we can propably say we do not
                    //pass the same argument into a function twice, so
                    //if they are from the same function they must be the same
                    //argument to alias
                    if (isa<Argument>(m1->first)) {
                        if (isa<Argument>(m2->first)) {
                            return dyn_cast<Argument>(m1->first)->getParent() !=
                                dyn_cast<Argument>(m2->first)->getParent() ||
                                m1->first == m2->first;
                        }
                        else
                            continue;
                    } else if (isa<Argument>(m2->first))
                        continue;

                    //Check if the offset lists are the same
                    auto m1b = m1->second.begin();
                    auto m2b = m2->second.begin();
                    while (m1b != m1->second.end() &&
                           m2b != m2->second.end()) {
                        if (*m1b != -1 && *m2b != -1 && *m1b != *m2b) {
                            break;
                        }
                        m1b++;
                        m2b++;
                    }

                    // errs() << *(m1->first) << " [";
                    // for (auto it = m1->second.begin(),
                    //          et = m1->second.end();
                    //      it != et; ++it)
                    //     errs() << *it << ",";
                    // errs() << "]\n";
                    // errs() << *(m2->first) << " [";
                    // for (auto it = m2->second.begin(),
                    //          et = m2->second.end();
                    //      it != et; ++it)
                    //     errs() << *it << ",";
                    // errs() << "]\n";
                    if (m1b != m1->second.end() ||
                        m2b != m2->second.end()) {
                        // errs() << "Have different offset lists\n";
                        continue;
                    }//  else {
                    //     errs() << "Have the same offset list\n";
                    // }

                    //If the lists are the same, we must refer to the same
                    //mutex if the targets are the same
                    if (m1->first == m2->first)
                        return true;

                    //We can only recursively solve loads
                    LoadInst* m1_load = dyn_cast<LoadInst>(m1->first);
                    LoadInst* m2_load = dyn_cast<LoadInst>(m2->first);
                    if (m1_load && m2_load) {
                        // errs() << "Recurred into:\n";
                        // errs() << m1_load << "\n" << m2_load << "\n";
                        if (mutexAlias(m1_load->getPointerOperand(),
                                       m2_load->getPointerOperand()))
                            return true;
                        // errs() << "Returned from:\n";
                        // errs() << m1_load << "\n" << m2_load << "\n";
                    }
                }
            }
            return false;
        }
        
        // //Checks wether two pointer-type values Mut1 and Mut2 potentially
        // //point to the same mutex
        // bool mutexAlias(Value *mut1, Value *mut2) {
        //     set<pair<Value*,int> > mut1loads;
        //     set<pair<Value*,int> > mut2loads;
        //     try {
        //         mut1loads = possibleLoads(mut1);

        //     } catch (Value* error) {
        //         errs() << "Failed to resolve " << *error << "\nfrom " << *mut1 << "\n";
        //         throw mut1;
        //     }
        //     try {
        //         mut2loads = possibleLoads(mut2);
        //     } catch (Value* error) {
        //         errs() << "Failed to resolve " << *error << "\nfrom " << *mut2 << "\n";
        //         throw mut1;
        //     }

        //     // errs() << "Started mutex alias with :\n";
        //     // errs() << mut1 << "=" << mut1->getName() <<  " : " << typeid(*mut1).name() << "\n";
        //     // errs() << mut2 << "=" << mut2->getName() << " : " << typeid(*mut2).name() << "\n";

        //     for (set<pair<Value*,int> >::iterator m1 = mut1loads.begin(),
        //              m1end = mut1loads.end(); m1 != m1end; ++m1) {
        //         for (set<pair<Value*,int> >::iterator m2 = mut2loads.begin(),
        //                  m2end = mut2loads.end(); m2 != m2end; ++m2) {
        //             if ((m1->second != -1 && m2->second != -1) &&
        //                 m1->second != m2->second)
        //                 continue;
        //             if (isa<Argument>(m1->first)) {
        //                 if (isa<Argument>(m2->first)) {
        //                     return true;
        //                 }
        //                 else
        //                     continue;
        //             } else if (isa<Argument>(m2->first))
        //                 continue;
        //             if (m1->first == m2->first)
        //                 return true;
        //             LoadInst* m1_load = dyn_cast<LoadInst>(m1->first);
        //             LoadInst* m2_load = dyn_cast<LoadInst>(m2->first);
        //             if (m1_load && m2_load) {
        //                 // errs() << "Resolved to :\n";
        //                 // errs() << m1_load << "=" << m1_load->getName() << "\n";
        //                 // errs() << m2_load << "=" << m2_load->getName() << "\n";
        //                 try {
        //                     if (mutexAlias(m1_load->getPointerOperand(),
        //                                    m2_load->getPointerOperand()))
        //                         return true;
        //                 } catch (Value *val) {
        //                     errs() << "Failed to resolve " << *val << "\nfrom " << *m1_load << "\nor" << *m2_load << "\n";
        //                     throw val;
        //                 }
        //             }
        //         }
        //     }
        //     return false;

        // }
        
        bool vectorContainsBB(SmallPtrSet<BasicBlock*, 128> vec, BasicBlock* BB){
            SmallPtrSet<BasicBlock*, 128>::iterator it = vec.begin();
            while (it != vec.end()){
                if ((*it) == BB)
                    return true;
                it++;
            }
            return false;
        }
    };
}

char xDRF::ID = 0;
static RegisterPass<xDRF> X("xDRF",
                            "Identify xDRF regions pass",
                            false,
                            false);

char SplitPThread::ID = 0;
static RegisterPass<SplitPThread> Y("split-calls",
                            "Split blocks around pthread calls pass",
                            false,
                            false);

/* Local Variables: */
/* mode: c++ */
/* indent-tabs-mode: nil */
/* c-basic-offset: 4 */
/* End: */
